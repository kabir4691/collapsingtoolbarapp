package com.kabir.example;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.kabir.example.utils.api.RetrofitInterface;
import com.squareup.okhttp.OkHttpClient;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    private final String BASE = "http://54.169.71.129";

    private final String POST_ID = "10228";
    private final String USER_ID = "6452";
    private final String LOGIN_KEY = "012d2ee03273a99f24a9ec264da020a4d6f1bfda66543ce66ad0b0a3b9f6192e";

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.textView)
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        collapsingToolbarLayout.setTitle("I am collapsing toolbar");

//        toolbar.setTitle("I am toolbar");
//        setSupportActionBar(toolbar);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();

        RetrofitInterface service = retrofit.create(RetrofitInterface.class);

        Call<JsonObject> productCall = service.getProductData(POST_ID, USER_ID, LOGIN_KEY);
        productCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Response<JsonObject> response, Retrofit retrofit) {

                Log.i(TAG, "onResponse");

                if (response == null) {
                    Log.e(TAG, "response is null");
                    return;
                }

                boolean isSucess = response.isSuccess();
                Log.d(TAG, "isSuccess : " + isSucess);

                JsonObject body = response.body();
                if (body == null) {
                    Log.e(TAG, "raw is null");
                    return;
                }

                Log.d(TAG, "base url : ".concat(retrofit.baseUrl().url().toString()));

//                textView.setText(body.toString());
            }

            @Override
            public void onFailure(Throwable throwable) {

                Log.i(TAG, "onFailure");

                if (throwable == null) {
                    Log.e(TAG, "throwable is null");
                    return;
                }

                String throwableString = throwable.toString();
                if (throwableString == null) {
                    Log.e(TAG, "throwableString is null");
                    return;
                }

                throwable.printStackTrace();
                textView.setText(throwable.getLocalizedMessage());
            }
        });
    }
}