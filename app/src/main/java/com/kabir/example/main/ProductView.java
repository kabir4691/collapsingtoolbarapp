package com.kabir.example.main;

import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;

public interface ProductView {

    String KEY_SOURCE = "from";
    String KEY_PRODUCT_ITEM = "productItem";
    String KEY_POST_ID = "post_id";
    String KEY_IS_FEATURED = "isFeatured";
    String KEY_ADDRESS_ID = "addressId";
    String KEY_IS_BEING_ADDED = "isBeingAdded";
    String KEY_SECONDS_TO_PREVIEW = "secondsToPreview";
    String KEY_PICTURE_COUNT = "pictureCount";
    String KEY_IS_LIKED = "is_liked";
    String KEY_POST_STATUS = "post_status";
    String KEY_COMMENTS = "comments";
    String KEY_COMMENTS_COUNT = "comments_count";

    String SOURCE_CART = "cart";
    String SOURCE_MY_CLOSET = "myCloset";
    String SOURCE_TRACK_ORDER = "trackOrder";
    String SOURCE_GCM = "gcm";
    String SOURCE_NOTIFICATION = "notification";
    String SOURCE_SEARCH = "search";
    String SOURCE_DEEP_LINK = "deepLink";
    String SOURCE_PROFILE = "profile";
    String SOURCE_FEATURED_PROFILE = "featuredProfile";
    String SOURCE_FEATURED_PRODUCT = "featuredProduct";
    String SOURCE_STYLE = "style";
    String SOURCE_SELL = "sell";

    String STATUS_UNAVAILABLE = "unavailable";
    String STATUS_UNAPPROVED = "unapproved";
    String STATUS_APPROVED = "approved";
    String STATUS_SOLD = "sold";
    String STATUS_BUY = "buy";
    String STATUS_CART = "cart";
    String STATUS_INCOMPLETE = "incomplete";
    String STATUS_REJECTED = "rejected";
    String STATUS_ONHOLD = "onhold";
    String STATUS_CANCELLED = "cancelled";
    String STATUS_DELETED = "deleted";
    String STATUS_PENDING = "pending";

    int RESULT_ADD_ANOTHER = 2, RESULT_ADD_EDIT_POST_FAIL = 3;

    int REQUEST_CODE_CART = 1, REQUEST_CODE_GALLERY = 2, REQUEST_CODE_PROFILE = 3,
            REQUEST_CODE_COMMENTS = 4, REQUEST_CODE_EDIT = 5, REQUEST_CODE_VERIFY_NUMBER = 6;

    void onFatalError();

    Context getViewContext();

    /**
     * Show a ProgressDialog with the given text.
     *
     * @param text The text to display as the content of the ProgressDialog.
     */
    void showProgressDialog(String text);

    /**
     * Hide the ProgressDialog.
     */
    void hideProgressDialog();

    void setToolBarTitle(String title);

    /**
     * Show/hide the ProgressBar to indicate loading of data.
     *
     * @param state true to show the ProgressBar, false to hide it.
     */
    void showLoading(boolean state);

    /**
     * Show/hide the button for the options menu at the top right of the product page.
     *
     * @param state true to show the button, false to hide it.
     */
    void showMenuButton(boolean state);

    /**
     * Show/hide the button for 'Confirm' at the top right of the product page (in case of
     * previewing a product before selling it).
     *
     * @param state true to show the button, false to hide it.
     */
    void showConfirmButton(boolean state);

    /**
     * Pop up the options menu at the top right of the product page.
     */
    void showPopupMenu();

    /**
     * Show the profile image of the seller.
     *
     * @param url The URL of the profile image.
     */
    void showProfileImage(String url);

    void previewProduct(ProductItem productItem);

    void updateProductStatus();

    void showSellerUserName(String username);

    void hideSellerUserName();

    /**
     * Scroll to the image at the specified position in the carousel.
     *
     * @param position     The position of the image in the carousel.
     * @param smoothScroll True if the scroll should be smooth, false otherwise.
     */
    void scrollToPositionInCarousel(int position, boolean smoothScroll);

    /**
     * Scroll to the thumbnail at the specified position in the row of thumbnails and mark it as
     * selected.
     *
     * @param position The position of the thumbnail in the row of thumbnails.
     */
    void scrollToPositionInThumbnail(int position);

    /**
     * Show the purchase price of the product.
     *
     * @param purchasePrice The purchase price.
     */
    void showPurchasePrice(int purchasePrice);

    /**
     * Hide the purchase price of the product.
     */
    void hidePurchasePrice();

    /**
     * Show the selling price of the product.
     *
     * @param sellingPrice The selling price.
     */
    void showSellingPrice(int sellingPrice);

    /**
     * Hide the selling price of the product.
     */
    void hideSellingPrice();

    /**
     * Show the discount of the product.
     *
     * @param sellingPrice  The selling price.
     * @param purchasePrice The purchase price.
     */
    void showDiscount(int sellingPrice, int purchasePrice);

    /**
     * Hide the discount of the product.
     */
    void hideDiscount();

    /**
     * Show the duration since the product was added.
     *
     * @param timestamp The timestamp as received from the server. If null, then the duration is
     *                  hidden.
     */
    void showDuration(String timestamp);

    /**
     * Hide the duration since the product was added.
     */
    void hideDuration();

    /**
     * Show whether the seller is currently being followed or not.
     */
    void showFollowStatus(boolean status);

    void hideFollowStatus();

    /**
     * Show the product images.
     *
     * @param list         The ArrayList of {@link ProductImageItem}. If null or empty, then an
     *                     error image is shown.
     * @param galleryShown Whether the gallery should be shown on clicking of an image.
     */
    void showProductImages(ArrayList<ProductImageItem> list, boolean galleryShown);

    void hideProductImages();

    /**
     * Show/hide content relating to the product details.
     *
     * @param state true to show the content, false to hide it.
     */
    void showContent(boolean state);

    /**
     * Update the number of users who have liked this product and change the text indicating the
     * same accordingly.
     */
    void updateLikesCount();

    /**
     * Show the user actions i.e. like, comment and buy buttons.
     *
     * @param state true to show the user actions, false to hide it.
     */
    void showUserActions(boolean state);

    /**
     * Show when the product was last updated.
     *
     * @param timestamp The timestamp as received from the server.
     */
    void showLastUpdatedOn(String timestamp);

    /**
     * Hide when the product was last updated.
     */
    void hideLastUpdatedOn();

    void updateCommentsList();

    /**
     * Show the product description.
     *
     * @param description The product description.
     */
    void showDescription(CharSequence description);

    /**
     * Hide the product description.
     */
    void hideDescription();

    /**
     * Show an error message in the center of the page after hiding all other data.
     *
     * @param error The message to display.
     */
    void showError(String error);

    /**
     * Hide the error message.
     */
    void hideError();

    void navigateToEditPost();

    void navigateToVerifyNumber(String number);

    void showReportProductDialog();
    void showReportProductSuccessDialog();
    void showReportProductFailDialog();

    void showDetails(boolean state);

    void showProductTitle(String title);
    void hideProductTitle();

    void addTagToList(String text, int resId, ArrayList<TextView> list);
    void showTags(ArrayList<TextView> list);
    void hideTags();

    void updateLikeStatus();

    void showCategory(String category);
    void hideCategory();

    void removeReportFromMenu();
    void removeEditFromMenu();

    void showAddProductSuccessDialog(String number, boolean isNumberVerified);

    void sendResultBackToCallingActivity(int resultCode);

    void navigateToCart();
    void navigateToProfile();
}
