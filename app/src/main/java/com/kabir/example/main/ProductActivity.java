package com.kabir.example.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kabir.example.R;
import com.kabir.example.carousel.CarouselPagerAdapter;
import com.kabir.example.thumbnail.ThumbnailAdapter;
import com.kabir.example.thumbnail.ThumbnailAdapter.CustomOnItemClickListener;
import com.kabir.example.thumbnail.ThumbnailItemDecoration;
import com.kabir.example.utils.AppConfig;
import com.kabir.example.utils.BitmapUtils;
import com.kabir.example.utils.CommentItem;
import com.kabir.example.utils.DialogUtils;
import com.kabir.example.utils.IntegerUtils;
import com.kabir.example.utils.StringUtils;
import com.kabir.example.utils.TypefaceUtils;
import com.kabir.example.utils.image.ImageProvider;
import com.kabir.example.utils.image.picasso.PicassoImageProvider;
import com.kabir.example.widgets.CustomCirclePageIndicator;
import com.kabir.example.widgets.FollowButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductActivity extends AppCompatActivity implements ProductView, OnClickListener {

    private final String TAG = "ProductActivity";

    @Bind(R.id.iv_menu)
    ImageView mMenuImage;

    @OnClick(R.id.iv_menu)
    void menuOnClick() {
        showPopupMenu();
    }

    @Bind(R.id.rl_actions)
    RelativeLayout mActionsLayout;
    @Bind(R.id.rl_details)
    RelativeLayout mDetailsLayout;

    @OnClick(R.id.rl_faq)
    void faqOnClick() {
//        Intent intent = new Intent(ProductActivity.this, FAQActivity.class);
//        intent.putExtra(FAQView.KEY_TITLE, FAQConstants.FAQ);
//        startActivity(intent);
    }

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.carousel_layout)
    FrameLayout mCarouselLayout;
    @Bind(R.id.tv_confirm)
    TextView mConfirmView;

    @OnClick(R.id.tv_confirm)
    void confirmOnClick() {

        if (mPresenter == null) {
            Log.e(TAG, "presenter is null");
            return;
        }

        float secondsToPreview = mPresenter.getSecondsToPreview();
        int pictureCount = mPresenter.getPictureCount();
        boolean isBeingAdded = mPresenter.isBeingAdded();
        String addressId = mPresenter.getAddressId();
        mPresenter.addOrEditProduct(secondsToPreview, pictureCount, isBeingAdded, addressId,
                                    mPresenter.getProductItem());
    }

    @Bind(R.id.tv_error)
    TextView mErrorView;
    @Bind(R.id.tv_status)
    TextView mStatusView;
    @Bind(R.id.tv_timestamp)
    TextView mTimestampView;
    @Bind(R.id.tv_username)
    TextView mUsernameView;
    @Bind(R.id.tv_price)
    TextView mPriceView;
    @Bind(R.id.tv_purchasePrice)
    TextView mPurchasePriceView;
    @Bind(R.id.tv_discount)
    TextView mDiscountView;
    @Bind(R.id.tv_title)
    TextView mTitleView;
    @Bind(R.id.tv_tags)
    TextView mTagsView;
    @Bind(R.id.tv_likes)
    TextView mLikesView;
    @Bind(R.id.tv_usernameAndDescription)
    TextView mUsernameAndDescriptionView;
    @Bind(R.id.tv_category)
    TextView mCategoryView;
    @Bind(R.id.tv_updated)
    TextView mUpdatedView;
    @Bind(R.id.tv_toggle)
    TextView mToggleView;

    FollowButton mFollowButton;

    @Bind(R.id.pb_loading)
    ProgressBar mLoadingView;
    @Bind(R.id.sv_content)
    NestedScrollView mNestedScrollView;
    @Bind(R.id.vp_carousel)
    ViewPager mCarouselPager;
    @Bind(R.id.cpi_carousel)
    CustomCirclePageIndicator mCarouselIndicator;

    @OnClick(R.id.iv_back)
    void backOnClick() {
        onBackPressed();
    }

    @Bind(R.id.iv_like)
    ImageView mLikeImage;

    @OnClick(R.id.iv_comment)
    void CommentOnClick() {
//        Intent intent = new Intent(ProductActivity.this, CommentsActivity.class);
//        intent.putExtra(CommentsView.KEY_POST_ID, mPresenter.getPostId());
//        intent.putExtra(CommentsView.KEY_IS_LIKED, mPresenter.isLiked());
//        startActivityForResult(intent, REQUEST_CODE_COMMENTS);
    }

    @Bind(R.id.iv_profile)
    ImageView mProfileImage;

    @OnClick(R.id.iv_profile)
    void profileOnClick() {

        if (mPresenter == null) {
            Log.e(TAG, "presenter is null");
            return;
        }

        String source = mPresenter.getSource();
        if (source != null && source.equals(SOURCE_SELL)) {
            return;
        }
        navigateToProfile();
    }

    @Bind(R.id.iv_error)
    ImageView mErrorImage;
    @Bind(R.id.iv_line2)
    View mBelowTitleLineView;
    @Bind(R.id.iv_line3)
    View mBelowTagsLineView;
    @Bind(R.id.rv_thumbnails)
    RecyclerView mThumbnailsRecyclerView;
    @Bind(R.id.lv_comments)
    ListView mCommentsListView;

    private PopupMenu mPopupMenu;
    private MaterialDialog mProgressDialog;
    private ThumbnailAdapter mThumbnailAdapter;

    private ProductPresenter mPresenter;
    private Resources mResources;
    private ImageProvider mImageProvider;

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    public void onFatalError() {
        onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mResources = getResources();
        mImageProvider = new PicassoImageProvider(this);
        overridePendingTransition(
                R.anim.activity_slide_left_enter, R.anim.activity_slide_left_exit);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);
        mFollowButton = (FollowButton) findViewById(R.id.button_follow);

        mCollapsingToolbarLayout.setCollapsedTitleTypeface(TypefaceUtils.getLato400Typeface(this));
        mCollapsingToolbarLayout.setExpandedTitleTypeface(TypefaceUtils.getLato400Typeface(this));

        mProgressDialog = new MaterialDialog.Builder(this)
                .progress(true, 0)
                .cancelable(false)
                .build();

        mPopupMenu = new PopupMenu(this, mMenuImage);
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_product, mPopupMenu.getMenu());
        mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.item_edit:
                        navigateToEditPost();
                        break;
                    case R.id.item_report:
                        showReportProductDialog();
                        break;
                }
                return true;
            }
        });

        mCarouselPager.setOffscreenPageLimit(1);
        mCarouselPager.setPageMargin(mResources.getDimensionPixelSize(
                R.dimen.margin_product_vp_carousel_page));

        mCarouselIndicator.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                scrollToPositionInThumbnail(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mThumbnailsRecyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mThumbnailsRecyclerView.addItemDecoration(
                new ThumbnailItemDecoration(
                        mResources.getDimensionPixelSize(R.dimen.inset_product_rv_thumbnails)));
        mThumbnailsRecyclerView.setHasFixedSize(true);
        mTagsView.setMovementMethod(LinkMovementMethod.getInstance());
        mUsernameAndDescriptionView.setMovementMethod(LinkMovementMethod.getInstance());

        mStatusView.setOnClickListener(this);
        mToggleView.setOnClickListener(this);
        mLikeImage.setOnClickListener(this);
        mFollowButton.setOnClickListener(this);

        mPresenter = new ProductPresenterImpl(this);
        Bundle bundle = new Bundle();
        bundle.putString(ProductView.KEY_POST_ID, "10950");
//        if (mPresenter.attachView(getIntent().getExtras())) {
        if (mPresenter.attachView(bundle)) {
            mPresenter.loadData();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    @Override
    public void showProgressDialog(String text) {
        DialogUtils.showProgressDialog(mProgressDialog, text);
    }

    @Override
    public void hideProgressDialog() {
        DialogUtils.hideProgressDialog(mProgressDialog);
    }

    @Override
    public void setToolBarTitle(String title) {
        mCollapsingToolbarLayout.setTitle(title.toUpperCase());
    }

    @Override
    public void showLoading(boolean state) {
        mLoadingView.setVisibility(state ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showMenuButton(boolean state) {
        mMenuImage.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPopupMenu() {
        mPopupMenu.show();
    }

    @Override
    public void showConfirmButton(boolean state) {
        mConfirmView.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProfileImage(String imageUrl) {
        mImageProvider.setPlaceHolderResourceId(R.drawable.image_placeholder_profile);
        mImageProvider.displayImage(
                imageUrl,
                mResources.getDimensionPixelSize(R.dimen.widthAndHeight_product_iv_profile),
                mResources.getDimensionPixelSize(R.dimen.widthAndHeight_product_iv_profile),
                mProfileImage);
    }

    @Override
    public void previewProduct(ProductItem productItem) {

        showMenuButton(false);
        showConfirmButton(true);
        showContent(false);
        hideError();
        showLoading(true);
        updateProductStatus();
        showUserActions(false);
        hideDuration();
        hideFollowStatus();
        hideLastUpdatedOn();
        updateLikesCount();

        showDetails(false);
        updateCommentsList();

        String profileImageUrl = productItem.getSellerProfileImageUrl();
        showProfileImage(profileImageUrl);

        showSellerUserName(productItem.getSellerUserName());

        ArrayList<ProductImageItem> imagesList = new ArrayList<>();
        String picture0 = productItem.getCoverPhotoUrl();
        if (picture0 != null) {
            imagesList.add(new ProductImageItem(picture0, picture0, picture0));
        }
        String picture1 = productItem.getOtherPhoto1Url();
        if (picture1 != null) {
            imagesList.add(new ProductImageItem(picture1, picture1, picture1));
        }
        String picture2 = productItem.getOtherPhoto2Url();
        if (picture2 != null) {
            imagesList.add(new ProductImageItem(picture2, picture2, picture2));
        }
        String picture3 = productItem.getOtherPhoto3Url();
        if (picture3 != null) {
            imagesList.add(new ProductImageItem(picture3, picture3, picture3));
        }
        String picture4 = productItem.getOtherPhoto4Url();
        if (picture4 != null) {
            imagesList.add(new ProductImageItem(picture4, picture4, picture4));
        }

        if (imagesList.size() == 0) {
            hideProductImages();
        } else {
            showProductImages(imagesList, false);
        }

        int sellingPrice = productItem.getSellingPrice();
        showSellingPrice(sellingPrice);

        int purchasePrice = productItem.getPurchasePrice();
        if (purchasePrice == 0) {
            hidePurchasePrice();
        } else {
            showPurchasePrice(purchasePrice);
        }

        if (sellingPrice == 0 || purchasePrice == 0) {
            hideDiscount();
        } else {
            showDiscount(sellingPrice, purchasePrice);
        }

        String title = productItem.getTitle();
        if (title == null || title.isEmpty()) {
            hideProductTitle();
        } else {
            showProductTitle(title);
        }

        ArrayList<TextView> tagsList = new ArrayList<>();
        String brand = productItem.getBrandName();
        if (brand != null) {
            addTagToList(Html.fromHtml(brand).toString(), R.drawable.ic_brand_dark_grey_12dp,
                         tagsList);
        }
        String color = productItem.getColorName();
        if (color != null) {
            addTagToList(color, R.drawable.ic_color_dark_grey_12dp, tagsList);
        }
        String size = productItem.getSizeName();
        if (size != null) {
            addTagToList(size, R.drawable.ic_size_dark_grey_12dp, tagsList);
        }
        boolean isNewWithTags = productItem.isNewWithTags();
        if (isNewWithTags) {
            addTagToList("New with Tags",
                         R.drawable.ic_new_with_tags_dark_grey_12dp,
                         tagsList);
        }

        if (tagsList.isEmpty()) {
            hideTags();
        } else {
            showTags(tagsList);
        }

        String description = productItem.getDescription();
        String[] lines = description.split("\n");
        description = "";
        for (String line : lines) {
            if (!line.isEmpty()) {
                description += StringUtils.fromHtml(line, "") + "\n";
            }
        }

        CharSequence desc = mPresenter
                .buildDescription(productItem.getSellerUserName(), description);
        if (desc.length() > 0) {
            showDescription(desc);
        } else {
            hideDescription();
        }

        String category = productItem.getCategoryName();
        if (category == null) {
            hideCategory();
        } else {
            showCategory(category);
        }

        showLoading(false);
        showContent(true);
    }

    @Override
    public void scrollToPositionInCarousel(int position, boolean smoothScroll) {

        if (mCarouselPager == null) {
            Log.e(TAG, "Carousel ViewPager is null");
            return;
        }
        mCarouselPager.setCurrentItem(position, smoothScroll);
    }

    @Override
    public void scrollToPositionInThumbnail(int position) {

        if (mThumbnailsRecyclerView == null) {
            Log.e(TAG, "thumbnails recyclerview is null");
            return;
        }

        if (mThumbnailAdapter == null) {
            Log.e(TAG, "thumbnails adapter is null");
            return;
        }

        mThumbnailsRecyclerView.smoothScrollToPosition(position);
        for (ProductImageItem item : mThumbnailAdapter.getImagesList()) {
            item.setIsSelected(false);
        }
        mThumbnailAdapter.getImagesList().get(position).setIsSelected(true);
        mThumbnailAdapter.notifyDataSetChanged();
    }

    @Override
    public void showSellerUserName(String username) {

        mUsernameView.setText(username);
        mUsernameView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSellerUserName() {
        mUsernameView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updateProductStatus() {

        String status = mPresenter.getProductStatus();

        if (status == null || status.isEmpty()) {
            Log.e(TAG, "product status is null or empty");
            mStatusView.setClickable(false);
            mStatusView.setVisibility(View.INVISIBLE);
            return;
        }

        mStatusView.setEnabled(false);
        mStatusView.setSelected(false);

        switch (status) {

            case STATUS_UNAVAILABLE:

                mStatusView.setText("Unavailable");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case STATUS_UNAPPROVED:

                mStatusView.setText("Unapproved");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case STATUS_APPROVED:

                mStatusView.setText("Approved");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case STATUS_SOLD:

                mStatusView.setText("Sold");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case STATUS_BUY:

                mStatusView.setText("Buy");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_add_to_bag_white_24dp, 0);
                mStatusView.setEnabled(true);
                mStatusView.setSelected(false);
                break;
            case STATUS_CART:

                mStatusView.setText("In Bag");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_bag_white_24dp, 0);
                mStatusView.setEnabled(true);
                mStatusView.setSelected(true);
                break;

            case STATUS_INCOMPLETE:
                mStatusView.setText("Incomplete post");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_edit_white_24dp, 0);
                mStatusView.setEnabled(true);
                mStatusView.setSelected(false);
                break;

            case STATUS_CANCELLED:

                mStatusView.setText("Cancelled");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case STATUS_REJECTED:

                mStatusView.setText("Rejected");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            case STATUS_DELETED:

                mStatusView.setText("Deleted");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            case STATUS_PENDING:

                mStatusView.setText("Pending Approval");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            case STATUS_ONHOLD:

                mStatusView.setText("On Hold");
                mStatusView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
        }

        mStatusView.setClickable(true);
        mStatusView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSellingPrice(int sellingPrice) {

        mPriceView.setText(StringUtils.RUPEE_SYMBOL + sellingPrice);
        mPriceView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSellingPrice() {
        mPriceView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showPurchasePrice(int purchasePrice) {

        mPurchasePriceView.setText(StringUtils.RUPEE_SYMBOL + String.valueOf(purchasePrice),
                                   BufferType.SPANNABLE);
        Spannable spannable = (Spannable) mPurchasePriceView.getText();
        spannable.setSpan(new StrikethroughSpan(), 0, mPurchasePriceView.getText().length(),
                          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mPurchasePriceView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePurchasePrice() {
        mPurchasePriceView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showDiscount(int sellingPrice, int purchasePrice) {

        mDiscountView.setText(
                "(" + IntegerUtils.calculateDiscount(sellingPrice, purchasePrice) + "% off)");
        mDiscountView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDiscount() {
        mDiscountView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showDuration(String timestamp) {

        mTimestampView.setText(StringUtils.calculateDurationFromTimestamp(timestamp));
        mTimestampView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDuration() {
        mTimestampView.setVisibility(View.GONE);
    }

    @Override
    public void showFollowStatus(boolean status) {

        String loggedInUserId = mPresenter.getUserAuth().getUserId();
        if (loggedInUserId == null || loggedInUserId.isEmpty()
                || loggedInUserId.equals(mPresenter.getSellerUserId())) {
            hideFollowStatus();
            return;
        }

        if (status) {
            mFollowButton.follow();
        } else {
            mFollowButton.unfollow();
        }
        mFollowButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFollowStatus() {
        mFollowButton.setVisibility(View.GONE);
    }

    @Override
    public void showProductImages(ArrayList<ProductImageItem> list, boolean galleryShown) {

        if (list == null || list.size() == 0) {
            Log.e(TAG, "product images list is null or empty");
            hideProductImages();
            return;
        }

        FragmentManager fm = getSupportFragmentManager();
        if (fm == null) {

            Log.e(TAG, "fragment manager is null");
            hideProductImages();
            return;
        }

        CarouselPagerAdapter carouselAdapter = new CarouselPagerAdapter(list, fm);
        carouselAdapter.setGalleryShown(galleryShown);

//        final ArrayList<GalleryItem> galleryList = new ArrayList<>();
//        for (ProductImageItem item : list) {
//            galleryList.add(new GalleryItem(
//                    item.getThumbnailUrl(), item.getLargeSizeUrl(), false, false));
//        }
//        carouselAdapter.setCallback(new CarouselPagerAdapter.Callback() {
//            @Override
//            public void onPageClick(int position) {
//
//                Intent intent = new Intent(ProductActivity.this, GalleryActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putParcelableArrayList(GalleryActivity.KEY_GALLERY_LIST, galleryList);
//                bundle.putInt(GalleryActivity.KEY_PAGE_POSITION, position);
//                intent.putExtras(bundle);
//                startActivityForResult(intent, REQUEST_CODE_GALLERY);
//            }
//        });
        mThumbnailAdapter = new ThumbnailAdapter(this, list);

        mCarouselPager.setAdapter(carouselAdapter);
        mCarouselIndicator.setViewPager(mCarouselPager);
        mThumbnailsRecyclerView.setAdapter(mThumbnailAdapter);

        mThumbnailAdapter.setOnItemClickListener(new CustomOnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                scrollToPositionInCarousel(position, true);
            }
        });

        scrollToPositionInCarousel(0, true);
        scrollToPositionInThumbnail(0);

        mErrorImage.setVisibility(View.INVISIBLE);
        mCarouselPager.setVisibility(View.VISIBLE);
        mCarouselIndicator.setVisibility(list.size() == 1 ? View.INVISIBLE : View.VISIBLE);
        mThumbnailsRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProductImages() {
        mCarouselPager.setVisibility(View.INVISIBLE);
        mCarouselIndicator.setVisibility(View.INVISIBLE);
        mThumbnailsRecyclerView.setVisibility(View.INVISIBLE);
        mErrorImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void showContent(boolean state) {

        if (state) {
            mCarouselLayout.setVisibility(View.VISIBLE);
            mNestedScrollView.setVisibility(View.VISIBLE);
        } else {
            mCarouselLayout.setVisibility(View.INVISIBLE);
            mNestedScrollView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showUserActions(boolean state) {
        mActionsLayout.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateCommentsList() {

        ArrayList<CommentItem> list = mPresenter.getCommentsList();

        if (list == null || list.isEmpty()) {
            mCommentsListView.setVisibility(View.GONE);
            return;
        }

        mCommentsListView.setAdapter(new ProductCommentAdapter(this, list));
        mCommentsListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLastUpdatedOn(String timestamp) {

        try {

            Date todayDate = new Date();
            Date postedDate = new SimpleDateFormat(AppConfig.FORMAT_TIMESTAMP).parse(timestamp);
            long elapsedTime = todayDate.getTime() - postedDate.getTime();
            String text;
            String prefix = "This item was updated ";
            if (TimeUnit.MILLISECONDS.toSeconds(elapsedTime) < 60) {
                text = prefix + String.valueOf(
                        TimeUnit.MILLISECONDS.toSeconds(elapsedTime)) + " seconds ago.";
            } else if (TimeUnit.MILLISECONDS.toMinutes(elapsedTime) < 60) {
                text = prefix + String.valueOf(
                        TimeUnit.MILLISECONDS.toMinutes(elapsedTime)) + " minutes ago.";
            } else if (TimeUnit.MILLISECONDS.toHours(elapsedTime) < 24) {
                text = prefix + String.valueOf(
                        TimeUnit.MILLISECONDS.toHours(elapsedTime)) + " hours ago.";
            } else if (TimeUnit.MILLISECONDS.toDays(elapsedTime) < 7) {
                text = prefix + String.valueOf(
                        TimeUnit.MILLISECONDS.toDays(elapsedTime)) + " days ago.";
            } else {
                text = prefix + String.valueOf(
                        TimeUnit.MILLISECONDS.toDays(elapsedTime) / 7) + " weeks ago.";
            }
            mUpdatedView.setText(text);
            mUpdatedView.setVisibility(View.VISIBLE);
        } catch (ParseException e) {

            e.printStackTrace();
            hideLastUpdatedOn();
        }
    }

    @Override
    public void hideLastUpdatedOn() {
        mUpdatedView.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {

        mErrorView.setText(error);
        mErrorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError() {
        mErrorView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void navigateToEditPost() {

//        Intent intent = new Intent(ProductActivity.this, SellActivity.class);
//        intent.putExtra(SellActivity.KEY_POST_ID, mPresenter.getPostId());
//        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }

    @Override
    public void navigateToVerifyNumber(String number) {

//        Intent intent = new Intent(ProductActivity.this, MobileVerificationActivity.class);
//        intent.putExtra(MobileVerificationView.KEY_MOBILE_NUMER, number);
//        intent.putExtra(MobileVerificationView.KEY_IS_VERIFIED, false);
//        startActivityForResult(intent, REQUEST_CODE_VERIFY_NUMBER);
    }

    @Override
    public void showReportProductDialog() {

        new MaterialDialog.Builder(this)
                .content(getString(R.string.report_product_confirm))
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        mPresenter.reportProduct();
                    }
                })
                .build().show();
    }

    @Override
    public void showReportProductSuccessDialog() {
        new MaterialDialog.Builder(this)
                .content(getString(R.string.report_product_success))
                .positiveText(getString(R.string.ok))
                .build().show();
    }

    @Override
    public void showReportProductFailDialog() {
        new MaterialDialog.Builder(this)
                .content(getString(R.string.report_product_fail))
                .positiveText(getString(R.string.ok))
                .build().show();
    }

    @Override
    public void showDetails(boolean state) {

        mToggleView.setText(getString(state ? R.string.hide_details : R.string.show_details));
        mDetailsLayout.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_status:

                switch (mPresenter.getProductStatus()) {

                    case STATUS_BUY:
                        mPresenter.addToCart();
                        break;
                    case STATUS_CART:
                        navigateToCart();
                        break;
                    case STATUS_INCOMPLETE:
                        navigateToEditPost();
                        break;
                }
                break;
            case R.id.button_follow:
                mPresenter.toggleFollow();
                break;
            case R.id.tv_toggle:
                showDetails(!isDetailsVisible());
                break;

            case R.id.iv_like:
                mPresenter.toggleLike();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_CODE_CART:

                    mPresenter.loadData();

                    // TODO Use Eventbus to update the cart (Cart gets updated automatically. So
                    // Keep this away for a while)
                    /*if (MainActivity.mCartFragment != null) {
                        MainActivity.mCartFragment.mPresenter.loadData(true);
                    }*/
                    break;
                case REQUEST_CODE_GALLERY:

//                    String positionString = resultIntent
//                            .getStringExtra(GalleryActivity.KEY_PAGE_POSITION);
//
//                    if (positionString != null) {
//
//                        int position = Integer.valueOf(positionString);
//                        ArrayList<ProductImageItem> list = mPresenter.getProductImagesList();
//                        if (list != null && position < list.size()) {
//
//                            scrollToPositionInCarousel(position, false);
//                            scrollToPositionInThumbnail(position);
//                        }
//                    }
                    break;
                case REQUEST_CODE_PROFILE:

//                    boolean followStatus = resultIntent.getBooleanExtra(
//                            ProfileActivity.KEY_IS_FOLLOWING, false);
//                    mPresenter.setIsSellerFollowed(followStatus);
//                    showFollowStatus(mPresenter.isSellerFollowed());
                    break;
                case REQUEST_CODE_COMMENTS:

//                    String postId = resultIntent.getStringExtra(CommentsView.KEY_POST_ID);

//                    ArrayList<CommentItem> commentsList = new ArrayList<>();
//
//                    ArrayList<CommentItem> temp = resultIntent
//                            .getParcelableArrayListExtra(CommentsView.KEY_COMMENTS_LIST);
//                    if (temp != null) {
//                        commentsList.addAll(temp);
//                    }
//
//                    mPresenter.setCommentsList(commentsList);
//                    updateCommentsList();
                    break;
                case REQUEST_CODE_EDIT:
                    mPresenter.loadData();
                    break;
                case REQUEST_CODE_VERIFY_NUMBER:

                    setResult(RESULT_OK);
                    finish();
//                    onBackPressed();
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {

            switch (requestCode) {

                case REQUEST_CODE_VERIFY_NUMBER:

                    setResult(RESULT_OK);
                    finish();
//                    onBackPressed();
                    break;
            }
        }
//        else if (resultCode == CartActivity.RESULT_SUCCESFUL_TRANSACTION) {
//
//            switch (requestCode) {
//
//                case REQUEST_CODE_CART:
//
//                    // Go to Main Activity
//                    Intent intent1 = new Intent(this, MainActivity.class);
//                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent1);
//
//                    // TODO Reload the Cart Fragment (Cart gets updated automatically. So keep
//                    // this away for a while)
//
//                    Intent intent2 = new Intent(this, CheckoutSuccessActivity.class);
//                    if (resultIntent.getExtras() != null) {
//                        intent2.putExtras(resultIntent.getExtras());
//                    }
//                    startActivity(intent2);
//                    break;
//            }
//        }
    }

    @Override
    public void showProductTitle(String title) {

        mTitleView.setText(Html.fromHtml(StringUtils.capWords(title)));
        mTitleView.setVisibility(View.VISIBLE);
        mBelowTitleLineView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProductTitle() {

        mTitleView.setVisibility(View.GONE);
        mBelowTitleLineView.setVisibility(View.GONE);
    }

    /**
     * Add a tag(TextView) to the list of tags.
     *
     * @param text  The text of the TextView.
     * @param resId The resource ID of the drawable to be used as the left compound drawable of
     *              the TextView.
     * @param list  The ArrayList of TextViews containing the tags.
     */
    @Override
    public void addTagToList(String text, int resId, ArrayList<TextView> list) {

        TextView tag = (TextView) getLayoutInflater()
                .inflate(R.layout.textview_product_tv_tags_item, null);
        tag.setText(text);
        tag.setCompoundDrawablesWithIntrinsicBounds(resId, 0, 0, 0);
        list.add(tag);
    }

    /**
     * Display the tags in the given list.
     *
     * @param list The ArrayList of TextViews that contain the tags.
     */
    @Override
    public void showTags(ArrayList<TextView> list) {

        SpannableStringBuilder builder = new SpannableStringBuilder();
        BitmapDrawable bd;
        for (int i = 0; i < list.size(); i++) {

            bd = BitmapUtils.convertViewToBitmapDrawable(list.get(i));
            bd.setBounds(0, 0, bd.getIntrinsicWidth(), bd.getIntrinsicHeight());
            builder.append(list.get(i).getText());
            builder.setSpan(new ImageSpan(bd), builder.length() - (list
                                    .get(i).getText().length()), builder.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        mTagsView.setText(builder);
        mTagsView.setVisibility(View.VISIBLE);
        mBelowTagsLineView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTags() {

        mTagsView.setVisibility(View.GONE);
        mBelowTagsLineView.setVisibility(View.GONE);
    }

    @Override
    public void showDescription(CharSequence description) {

        mUsernameAndDescriptionView.setText(description);
        mUsernameAndDescriptionView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDescription() {
        mUsernameAndDescriptionView.setVisibility(View.GONE);
    }

    @Override
    public void showCategory(String category) {
        mCategoryView.setText(category);
        mCategoryView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCategory() {
        mCategoryView.setVisibility(View.GONE);
    }

    @Override
    public void updateLikesCount() {
        mLikesView.setText(StringUtils.getTextForLikesCount(
                mPresenter.getLikesCount(), mPresenter.isLiked()));
    }

    @Override
    public void updateLikeStatus() {
        mLikeImage.setSelected(mPresenter.isLiked());
    }

    @Override
    public void removeReportFromMenu() {

        if (mPopupMenu == null) {
            Log.e(TAG, "pop up menu is null");
            return;
        }
        mPopupMenu.getMenu().removeItem(R.id.item_report);
    }

    @Override
    public void removeEditFromMenu() {

        if (mPopupMenu == null) {
            Log.e(TAG, "pop up menu is null");
            return;
        }
        mPopupMenu.getMenu().removeItem(R.id.item_edit);
    }

    @Override
    public void showAddProductSuccessDialog(String number, boolean isNumberVerified) {

        final MaterialDialog dialog = new MaterialDialog
                .Builder(ProductActivity.this)
                .cancelable(false)
                .customView(R.layout.view_product_dialog_add_success, true)
                .build();

        View view = dialog.getCustomView();
        if (view == null) {

            Log.e(TAG, "dialog view is null");
            setResult(RESULT_OK);
            onBackPressed();
            return;
        }
        ImageView iv_close = (ImageView) view.findViewById(R.id.iv_close);
        TextView tv_message2 = (TextView) view.findViewById(R.id.tv_message2);
        TextView tv_action = (TextView) view.findViewById(R.id.tv_action);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                onBackPressed();
            }
        });

        if (isNumberVerified) {

//            tv_message2.setText(R.string.sell_create_post_hint_verified);
            tv_action.setText("Add another product");
            tv_action.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sendResultBackToCallingActivity(RESULT_ADD_ANOTHER);
                }
            });

        } else {

//            tv_message2.setText(R.string.sell_create_post_hint_unverified);
//            tv_action.setText(getString(R.string.verify_number));
//            final String finalNumber = number;
//            tv_action.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    navigateToVerifyNumber(finalNumber);
//                }
//            });
        }

        dialog.show();
    }

    @Override
    public void sendResultBackToCallingActivity(int resultCode) {
        setResult(resultCode);
        onBackPressed();
    }

    /**
     * @return Whether the details are currently visible or not
     */
    boolean isDetailsVisible() {
        return mDetailsLayout.getVisibility() == View.VISIBLE;
    }

    /**
     * Finish the activity with a custom transition. super.onBackPressed is not called here on
     * purpose.
     */
    @Override
    public void onBackPressed() {

        if (mPresenter != null && mPresenter.getPostId() != null &&
                mPresenter.isProductDetailsLoaded()) {
            Intent intent = new Intent();
            Log.i(TAG, "postid for onActivityResult: " + mPresenter.getPostId());

            intent.putExtra(KEY_POST_ID, mPresenter.getPostId());
            intent.putExtra(KEY_IS_LIKED, mPresenter.isLiked());

            if (mPresenter.getProductStatus() != null) {
                intent.putExtra(KEY_POST_STATUS, mPresenter.getProductStatus());
            }

            ArrayList<CommentItem> commentsList = mPresenter.getCommentsList();

            if (commentsList != null) {

                ArrayList<CommentItem> comments = new ArrayList<>();
                for (int i = commentsList.size() - 1, j = 0; i >= 0 && j < 2; i--, j++) {
                    Log.i(TAG, "adding comment: " + j);
                    comments.add(commentsList.get(i));
                    j++;
                }

                intent.putParcelableArrayListExtra(KEY_COMMENTS, comments);
                intent.putExtra(KEY_COMMENTS_COUNT, commentsList.size());

            } else {
                Log.e(TAG, "comments list is null");
            }


            Log.i(TAG, "set result for onActivityResult");
            setResult(RESULT_OK, intent);
        }

        super.onBackPressed();
        overridePendingTransition(
                R.anim.activity_slide_right_enter, R.anim.activity_slide_right_exit);
    }

    @Override
    public void navigateToCart() {

//        Intent intent = new Intent(this, CartActivity.class);
//        startActivityForResult(intent, REQUEST_CODE_CART);
    }

    @Override
    public void navigateToProfile() {

//        Intent intent = new Intent(ProductActivity.this, ProfileActivity.class);
//        intent.putExtra(ProfileView.KEY_USER_ID, mPresenter.getSellerUserId());
//        if (mPresenter.isFeatured()) {
//            intent.putExtra(ProfileView.KEY_SOURCE, ProfileView.SOURCE_FEATURED_PRODUCT);
//        } else {
//            intent.putExtra(ProfileView.KEY_SOURCE, ProfileView.SOURCE_PRODUCT);
//        }
//        startActivityForResult(intent, REQUEST_CODE_PROFILE);
    }
}
