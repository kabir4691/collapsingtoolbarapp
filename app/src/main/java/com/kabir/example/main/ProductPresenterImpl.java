package com.kabir.example.main;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.Patterns;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kabir.example.R;
import com.kabir.example.utils.api.ApiCall;
import com.kabir.example.utils.api.ApiProvider;
import com.kabir.example.utils.api.ApiResponse;
import com.kabir.example.utils.CommentItem;
import com.kabir.example.utils.ConnectivityUtils;
import com.kabir.example.utils.CustomTypefaceSpan;
import com.kabir.example.utils.CustomURLSpan;
import com.kabir.example.utils.api.RetrofitApiProvider;
import com.kabir.example.utils.StringUtils;
import com.kabir.example.utils.ToastUtils;
import com.kabir.example.utils.TypefaceUtils;
import com.kabir.example.utils.UserAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

public class ProductPresenterImpl implements ProductPresenter {

    private final String TAG = "ProductPresenterImpl";

    private ProductView view;
    private Resources resources;

    private ProductItem productItem;
    private String postId;
    private String source;
    private boolean isFeatured;
    private float secondsToPreview;
    private int pictureCount;
    private boolean isBeingAdded;
    private String addressId;
    private String sellerUserId;
    private String sellerUserName;
    private int likesCount;
    private boolean isLiked;
    private boolean isProductDetailsLoaded;
    private ArrayList<CommentItem> commentsList;
    private String productStatus;
    private long confirmStartTime;
    private boolean isSellerFollowed;
    private boolean isLikeBeingToggled;
    private ArrayList<ProductImageItem> productImagesList;

    private Handler handler;
    private Runnable followRunnable;
    private final int FOLLOW_WAIT = 1200;
    private Runnable likeRunnable;
    private final int LIKE_WAIT = 1200;

    private ApiCall productDetailsApiCall;
    private ApiCall toggleProductLikeApiCall;
    private ApiCall toggleUserFollowApiCall;
    private ApiCall userDetailsApiCall;
    private ApiCall commentsListApiCall;
    private ApiCall addProductToCartApiCall;
    private ApiCall reportProductApiCall;
    private ApiCall addOrEditProductApiCall;

    private ApiProvider apiProvider;
    private UserAuth userAuth;

    public ProductPresenterImpl(ProductView view) {
        this.view = view;
        resources = view.getViewContext().getResources();
    }

    @Override
    public boolean attachView(Bundle extras) {

        if (extras == null || extras.isEmpty()) {
            Log.e(TAG, "extras is null or empty");
            view.onFatalError();
            return false;
        }

        setPostId(extras.getString(ProductView.KEY_POST_ID));

        if (getPostId() == null) {
            Log.e(TAG, "post ID is null");
            view.onFatalError();
            return false;
        }

        setProductItem(extras.<ProductItem>getParcelable(ProductView.KEY_PRODUCT_ITEM));
        setSource(extras.getString(ProductView.KEY_SOURCE));
        setIsFeatured(extras.getBoolean(ProductView.KEY_IS_FEATURED));
        setSecondsToPreview(extras.getFloat(ProductView.KEY_SECONDS_TO_PREVIEW, -1));
        setPictureCount(extras.getInt(ProductView.KEY_PICTURE_COUNT, -1));
        setIsBeingAdded(extras.getBoolean(ProductView.KEY_IS_BEING_ADDED));
        setAddressId(extras.getString(ProductView.KEY_ADDRESS_ID));
        setIsLikeBeingToggled(false);
        setIsLiked(false);
        setIsSellerFollowed(false);
        setIsProductDetailsLoaded(false);

        setUserAuth(new UserAuth(
                "6452", "012d2ee03273a99f24a9ec264da020a4d6f1bfda66543ce66ad0b0a3b9f6192e"));
        apiProvider = new RetrofitApiProvider();

        handler = new Handler();
        followRunnable = new Runnable() {
            @Override
            public void run() {
                toggleUserFollowApiCall = apiProvider.toggleUserFollow(
                        isSellerFollowed(), getSellerUserId(), getUserAuth(),
                        new ToggleUserFollowApiListener());
            }
        };
        likeRunnable = new Runnable() {
            @Override
            public void run() {
                setIsLikeBeingToggled(true);
                toggleProductLikeApiCall = apiProvider.toggleProductLike(
                        isLiked(), getPostId(), getUserAuth(), new ToggleProductLikeApiListener());
            }
        };

        return true;
    }

    @Override
    public void detachView() {

        if (productDetailsApiCall != null) {
            productDetailsApiCall.cancel();
        }
        if (userDetailsApiCall != null) {
            userDetailsApiCall.cancel();
        }
        if (commentsListApiCall != null) {
            commentsListApiCall.cancel();
        }
        if (toggleProductLikeApiCall != null) {
            toggleProductLikeApiCall.cancel();
        }
        if (toggleUserFollowApiCall != null) {
            toggleUserFollowApiCall.cancel();
        }
        if (addProductToCartApiCall != null) {
            addProductToCartApiCall.cancel();
        }
        if (reportProductApiCall != null) {
            reportProductApiCall.cancel();
        }
        if (addOrEditProductApiCall != null) {
            addOrEditProductApiCall.cancel();
        }
    }

    @Override
    public CharSequence buildDescription(String username, String description) {

        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (username == null || username.isEmpty() || description == null || description
                .isEmpty()) {
            Log.e(TAG, "username or description is null or empty");
        } else {

            builder.append(username.concat(" "));
            builder.setSpan(
                    new TextAppearanceSpan(view.getViewContext(), R.style.ProductUsername2Style),
                    builder.length() - (username.concat(" ")).length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(
                    new CustomTypefaceSpan(
                            null, TypefaceUtils.getLato700Typeface(view.getViewContext())),
                    builder.length() - (username.concat(" ")).length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            builder.append(description);
            builder.setSpan(
                    new TextAppearanceSpan(view.getViewContext(), R.style.ProductDescriptionStyle),
                    builder.length() - description.length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(
                    new CustomTypefaceSpan(
                            null, TypefaceUtils.getLato400Typeface(view.getViewContext())),
                    builder.length() - description.length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            Matcher matcher = Patterns.WEB_URL.matcher(description);
            while (matcher.find()) {

                int descriptionStart = builder.length() - description.length();
                int urlStart = matcher.start();
                int urlEnd = matcher.end();

                String url = description.subSequence(urlStart, urlEnd).toString();

                CustomURLSpan urlSpan = new CustomURLSpan(url);

                builder.setSpan(
                        urlSpan, descriptionStart + urlStart, descriptionStart + urlEnd,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return builder;
    }

    @Override
    public void loadData() {

        if (getSource() != null && getSource().equals(ProductView.SOURCE_SELL)) {

            if (getProductItem() == null) {
                Log.e(TAG, "ProductItem is null");
                view.onFatalError();
                return;
            }

            setConfirmStartTime(System.currentTimeMillis());

            setProductStatus(null);
            setLikesCount(0);
            setCommentsList(null);
            setSellerUserId(getProductItem().getSellerUserId());
            setSellerUserName(getProductItem().getSellerUserName());

            ArrayList<ProductImageItem> imagesList = new ArrayList<>();
            String picture0 = getProductItem().getCoverPhotoUrl();
            if (picture0 != null) {
                imagesList.add(new ProductImageItem(picture0, picture0, picture0));
            }
            String picture1 = getProductItem().getOtherPhoto1Url();
            if (picture1 != null) {
                imagesList.add(new ProductImageItem(picture1, picture1, picture1));
            }
            String picture2 = getProductItem().getOtherPhoto2Url();
            if (picture2 != null) {
                imagesList.add(new ProductImageItem(picture2, picture2, picture2));
            }
            String picture3 = getProductItem().getOtherPhoto3Url();
            if (picture3 != null) {
                imagesList.add(new ProductImageItem(picture3, picture3, picture3));
            }
            String picture4 = getProductItem().getOtherPhoto4Url();
            if (picture4 != null) {
                imagesList.add(new ProductImageItem(picture4, picture4, picture4));
            }
            setProductImagesList(imagesList);

            view.setToolBarTitle("Product Preview");
            view.previewProduct(getProductItem());
        } else {

            view.setToolBarTitle("Product Details");
            view.showMenuButton(false);
            view.showConfirmButton(false);
            view.showUserActions(false);
            view.showContent(false);
            view.hideError();
            view.hideFollowStatus();
            setCommentsList(null);
            view.updateCommentsList();
            view.showDetails(false);
            loadProductData();
        }
    }

    @Override
    public void loadProductData() {

        if (ConnectivityUtils.isConnectedToInternet()) {

            view.showLoading(true);
            productDetailsApiCall = apiProvider.getProductDetails(
                    getPostId(), getUserAuth(), new ProductDetailsApiListener());
        } else {
            view.showError(resources.getString(R.string.internet_error));
        }
    }

    private class ProductDetailsApiListener implements ApiProvider.ProductDetailsListener {

        @Override
        public void onSuccess(JsonObject jsonObject) {

            try {

                if (!jsonObject.has(ApiResponse.KEY_CONTENT)) {
                    // content not available;
                    Log.e(TAG, "content is not present");
                    onFail("content is not present");
                    return;
                }

                Gson gson = new Gson();
                String JSONString = gson.toJson(jsonObject);
                JSONObject mainJSON = new JSONObject(JSONString);

                JSONObject jsonContent = mainJSON.getJSONObject(ApiResponse.KEY_CONTENT);

                if (!jsonContent.has(ApiResponse.KEY_USER_ID)) {
                    // user id not present;
                    Log.e(TAG, "user id is not present");
                    onFail("user id is not present");
                    return;
                }

                setSellerUserId(jsonContent.getString(ApiResponse.KEY_USER_ID));

                if (!jsonContent.has(ApiResponse.KEY_AVAILABLE)) {
                    // field not present;
                    Log.e(TAG, ApiResponse.KEY_AVAILABLE + " is not present");
                    onFail(ApiResponse.KEY_AVAILABLE + " is not present");
                    return;
                }
                int status = jsonContent.getInt(ApiResponse.KEY_AVAILABLE);

                if (!jsonContent.has(ApiResponse.KEY_POST_STATUS)) {
                    // post status not available
                    // do something here
                    Log.e(TAG, ApiResponse.KEY_POST_STATUS + " is not present");
                    onFail(ApiResponse.KEY_POST_STATUS + " is not present");
                    return;
                }
                String postStatus = jsonContent.getString(ApiResponse.KEY_POST_STATUS);

                if (!jsonContent.has(ApiResponse.KEY_ISCART)) {
                    // cart field not present
                    // do something here
                    Log.e(TAG, ApiResponse.KEY_ISCART + " is not present");
                    onFail(ApiResponse.KEY_ISCART + " is not present");
                    return;
                }

                boolean addedInCart = jsonContent.getBoolean(ApiResponse.KEY_ISCART);

                switch (status) {
                    case 0:
                        setProductStatus(ProductView.STATUS_UNAVAILABLE);
                        break;

                    case 1:
                        if (ApiResponse.VALUE_POST_STATUS_SOLD.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_SOLD);
                        } else if (ApiResponse.VALUE_POST_STATUS_UNAPPROVED
                                .equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_UNAPPROVED);
                        } else if (ApiResponse.VALUE_POST_STATUS_APPROVED.equals(postStatus)) {

                            if (getUserAuth().getUserId().equals(getSellerUserId())) {
                                setProductStatus(ProductView.STATUS_APPROVED);
                            } else {

                                if (addedInCart) {
                                    setProductStatus(ProductView.STATUS_CART);
                                } else {
                                    setProductStatus(ProductView.STATUS_BUY);
                                }
                            }
                        } else if (ApiResponse.VALUE_POST_STATUS_INCOMPLETE.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_INCOMPLETE);
                        } else if (ApiResponse.VALUE_POST_STATUS_CANCELLED.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_CANCELLED);
                        } else if (ApiResponse.VALUE_POST_STATUS_DELETED.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_DELETED);
                        } else if (ApiResponse.VALUE_POST_STATUS_PENDING.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_PENDING);
                        } else if (ApiResponse.VALUE_POST_STATUS_ONHOLD.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_ONHOLD);
                        } else if (ApiResponse.VALUE_POST_STATUS_REJECTED.equals(postStatus)) {
                            setProductStatus(ProductView.STATUS_REJECTED);
                        } else {
                            setProductStatus(null);
                        }
                        break;
                }
                view.updateProductStatus();

                if (getUserAuth().getUserId().equals(getSellerUserId()) &&
                        ProductView.STATUS_SOLD.equals(getProductStatus())) {
                    view.showMenuButton(false);
                } else {
                    view.showMenuButton(true);
                }

                if (jsonContent.has(ApiResponse.KEY_TIMESTAMP)) {
                    view.showDuration(jsonContent.getString(ApiResponse.KEY_TIMESTAMP));
                } else {

                    Log.e(TAG, ApiResponse.KEY_TIMESTAMP + " is not present");
                    view.hideDuration();
                }

                int price = -1;
                if (jsonContent.has(ApiResponse.KEY_PRICE)) {

                    price = jsonContent.getInt(ApiResponse.KEY_PRICE);
                    view.showSellingPrice(price);
                } else {

                    Log.e(TAG, ApiResponse.KEY_PRICE + " is not present");
                    view.hideSellingPrice();
                }

                int purchasePrice = -1;
                if (jsonContent.has(ApiResponse.KEY_PURCHASE_PRICE)) {

                    purchasePrice = jsonContent.getInt(ApiResponse.KEY_PURCHASE_PRICE);
                    if (purchasePrice == 0) {
                        view.hidePurchasePrice();
                    } else {
                        view.showPurchasePrice(purchasePrice);
                    }
                } else {

                    Log.e(TAG, ApiResponse.KEY_PURCHASE_PRICE + " is not present");
                    view.hidePurchasePrice();
                }

                if (purchasePrice != -1 && purchasePrice != 0 && price != -1 && price != 0) {
                    view.showDiscount(price, purchasePrice);
                } else {
                    view.hideDiscount();
                }

                List<String> pictureIdList = Arrays.asList(
                        ApiResponse.KEY_PICTURE_0, ApiResponse.KEY_PICTURE_1,
                        ApiResponse.KEY_PICTURE_2, ApiResponse.KEY_PICTURE_3,
                        ApiResponse.KEY_PICTURE_4);
                ArrayList<ProductImageItem> imagesList = new ArrayList<>();
                for (String item : pictureIdList) {

                    if (!jsonContent.has(item)) {
                        Log.e(TAG, "content does not have: " + item);
                        continue;
                    }

                    JSONObject jsonImage = jsonContent.getJSONObject(item);

                    String thumbnail = jsonImage.optString(ApiResponse.KEY_THUMBNAIL, null);
                    String mediumSize = jsonImage.optString(ApiResponse.KEY_MEDIUM_SIZE, null);
                    String largeSize = jsonImage.optString(ApiResponse.KEY_LARGE_SIZE, null);

                    imagesList.add(new ProductImageItem(thumbnail, mediumSize, largeSize));
                }

                if (imagesList.size() == 0) {
                    view.hideProductImages();
                } else {
                    view.showProductImages(imagesList, true);
                }
                setProductImagesList(imagesList);

                if (jsonContent.has(ApiResponse.KEY_TITLE)) {
                    view.showProductTitle(jsonContent.getString(ApiResponse.KEY_TITLE));
                } else {
                    Log.e(TAG, ApiResponse.KEY_TITLE + " is not present");
                    view.hideProductTitle();
                }

                ArrayList<TextView> tagsList = new ArrayList<>();
                if (jsonContent.has(ApiResponse.KEY_BRAND)) {

                    String brand = Html.fromHtml(jsonContent.getString(ApiResponse.KEY_BRAND))
                                       .toString();
                    view.addTagToList(brand, R.drawable.ic_brand_dark_grey_12dp, tagsList);
                } else {
                    Log.e(TAG, ApiResponse.KEY_BRAND + " is not present");
                }

                if (jsonContent.has(ApiResponse.KEY_COLOR)) {
                    view.addTagToList(jsonContent.getString(ApiResponse.KEY_COLOR),
                                      R.drawable.ic_color_dark_grey_12dp, tagsList);
                } else {
                    Log.e(TAG, ApiResponse.KEY_COLOR + " is not present");
                }

                if (jsonContent.has(ApiResponse.KEY_SIZE)) {
                    view.addTagToList(jsonContent.getString(ApiResponse.KEY_SIZE),
                                      R.drawable.ic_size_dark_grey_12dp, tagsList);
                } else {
                    Log.e(TAG, ApiResponse.KEY_SIZE + " is not present");
                }

                if (!jsonContent.isNull(ApiResponse.KEY_NWT)) {

                    int nwtStatus = 0;
                    Object nwtIsSuchALoser = jsonContent.get(ApiResponse.KEY_NWT);
                    if (nwtIsSuchALoser instanceof Integer) {
                        nwtStatus = jsonContent.getInt(ApiResponse.KEY_NWT);
                    }

                    if (nwtStatus == 1) {
                        view.addTagToList("New with tags",
                                          R.drawable.ic_new_with_tags_dark_grey_12dp, tagsList);
                    }
                } else {
                    Log.e(TAG, ApiResponse.KEY_NWT + " is not present");
                }

                if (tagsList.isEmpty()) {
                    view.hideTags();
                } else {
                    view.showTags(tagsList);
                }

                boolean hasLiked = false;
                if (jsonContent.has(ApiResponse.KEY_IS_LIKED)) {
                    hasLiked = jsonContent.getBoolean(ApiResponse.KEY_IS_LIKED);
                } else {
                    Log.e(TAG, ApiResponse.KEY_IS_LIKED + " is not present");
                }
                setIsLiked(hasLiked);
                view.updateLikeStatus();

                int likes = 0;
                if (jsonContent.has(ApiResponse.KEY_LIKES)) {
                    likes = jsonContent.getInt(ApiResponse.KEY_LIKES);
                } else {
                    Log.e(TAG, ApiResponse.KEY_LIKES + " is not present");
                }
                setLikesCount(likes);
                view.updateLikesCount();

                String username = "";
                String description = "";

                if (jsonContent.has(ApiResponse.KEY_USERNAME)) {
                    username = jsonContent.getString(ApiResponse.KEY_USERNAME);
                } else {
                    Log.e(TAG, "username is not present");
                }
                username = StringUtils.fromHtml(username, "");

                if (jsonContent.has(ApiResponse.KEY_DESCRIPTION)) {
                    description = jsonContent.getString(ApiResponse.KEY_DESCRIPTION);
                } else {
                    Log.e(TAG, "description is not present");
                }

                String[] lines = description.split("\n");
                description = "";
                for (String line : lines) {
                    if (!line.isEmpty()) {
                        description += StringUtils.fromHtml(line, "") + "\n";
                    }
                }

                CharSequence desc = buildDescription(username, description);
                if (desc.length() > 0) {
                    view.showDescription(desc);
                } else {
                    view.hideDescription();
                }

                if (jsonContent.has(ApiResponse.KEY_CATEGORY_NAME)) {
                    view.showCategory(jsonContent.getString(ApiResponse.KEY_CATEGORY_NAME));
                } else {
                    Log.e(TAG, "category is not present");
                    view.hideCategory();
                }

                if (jsonContent.has(ApiResponse.KEY_TIMESTAMP)) {
                    view.showLastUpdatedOn(jsonContent.getString(ApiResponse.KEY_TIMESTAMP));
                } else {

                    Log.e(TAG, "timestamp is not present");
                    view.hideLastUpdatedOn();
                }

                setIsProductDetailsLoaded(true);

                loadSellerData();
            } catch (JSONException e) {

                e.printStackTrace();
                onFail("exception");
            }
        }

        @Override
        public void onFail(String errorMessage) {

            view.showLoading(false);
            view.showError(errorMessage);
        }
    }

    @Override
    public void loadSellerData() {

        if (ConnectivityUtils.isConnectedToInternet()) {
            userDetailsApiCall = apiProvider.getUserDetails(
                    getSellerUserId(), getUserAuth(), new SellerDetailsApiListener());
        } else {

            view.showLoading(false);
            view.showError(resources.getString(R.string.internet_error));
        }
    }

    private class SellerDetailsApiListener implements ApiProvider.UserDetailsListener {

        @Override
        public void onSuccess(JsonObject jsonObject) {

            try {

                Gson gson = new Gson();
                String JSONString = gson.toJson(jsonObject);
                JSONObject jsonResponse = new JSONObject(JSONString);

                if (!jsonResponse.has(ApiResponse.KEY_CONTENT)) {

                    Log.e(TAG, "content is not present");
                    onFail("content is not present");
                    return;
                }

                JSONObject jsonContent = jsonResponse.getJSONObject(ApiResponse.KEY_CONTENT);

                if (jsonContent.has(ApiResponse.KEY_PICTURE)) {
                    view.showProfileImage(jsonContent.getString(ApiResponse.KEY_PICTURE));
                } else {

                    Log.e(TAG, "picture is not present");
                    view.showProfileImage(null);
                }

                setSellerUserName(jsonContent.optString(ApiResponse.KEY_USERNAME, null));
                if (getSellerUserName() == null) {
                    Log.e(TAG, "username not available");
                    view.hideSellerUserName();
                } else {
                    view.showSellerUserName(getSellerUserName());
                }

                if (getUserAuth().getUserId().equals(getSellerUserId())) {

                    view.hideFollowStatus();
                    view.removeReportFromMenu();
                } else {

                    boolean isFollowing = false;
                    if (jsonContent.has(ApiResponse.KEY_IS_FOLLOWING)) {
                        isFollowing = jsonContent.getBoolean(ApiResponse.KEY_IS_FOLLOWING);
                    } else {
                        Log.e(TAG, ApiResponse.KEY_IS_FOLLOWING + " not present");
                    }
                    setIsSellerFollowed(isFollowing);
                    view.showFollowStatus(isSellerFollowed());
                    view.removeEditFromMenu();
                }

                loadCommentsData();
            } catch (JSONException e) {

                e.printStackTrace();
                onFail("fail");
            }
        }

        @Override
        public void onFail(String errorMessage) {

            view.showLoading(false);
            view.showError(errorMessage);
        }
    }

    @Override
    public void loadCommentsData() {

        if (ConnectivityUtils.isConnectedToInternet()) {
            commentsListApiCall = apiProvider.getProductComments(
                    getPostId(), getUserAuth(), new CommentsDataApiListener());
        } else {

            view.showLoading(false);
            view.showError(resources.getString(R.string.internet_error));
        }
    }

    private class CommentsDataApiListener implements ApiProvider.ProductCommentsListener {

        @Override
        public void onSuccess(JsonObject jsonObject) {

            try {

                Gson gson = new Gson();
                String JSONString = gson.toJson(jsonObject);
                JSONObject jsonResponse = new JSONObject(JSONString);

                ArrayList<CommentItem> list = new ArrayList<>();
                if (!jsonResponse.has(ApiResponse.KEY_CONTENT)) {
                    Log.e(TAG, "content is not present");
                } else {

                    JSONArray jsonContent = jsonResponse.getJSONArray(ApiResponse.KEY_CONTENT);
                    for (int i = 0; i < jsonContent.length(); i++) {

                        try {

                            JSONObject jsonComment = jsonContent.getJSONObject(i);
                            CommentItem commentItem = CommentItem.parseJSON(jsonComment);
                            list.add(commentItem);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                setCommentsList(list);
                view.updateCommentsList();

                view.showLoading(false);
                view.showUserActions(true);
                view.showContent(true);
            } catch (JSONException e) {

                e.printStackTrace();
                onFail("fail");
            }
        }

        @Override
        public void onFail(String errorMessage) {

            view.showLoading(false);
            view.showError(errorMessage);
        }
    }

    @Override
    public void addToCart() {

        if (ConnectivityUtils.isConnectedToInternet()) {

            view.showProgressDialog(resources.getString(R.string.loading_please_wait));
            addProductToCartApiCall = apiProvider.addProductToCart(
                    getPostId(), getUserAuth(), new AddProductToCartApiListener());
        } else {
            ToastUtils.showShortToast(resources.getString(R.string.internet_error));
        }
    }

    private class AddProductToCartApiListener implements ApiProvider
            .AddProductToCartListener {

        @Override
        public void onSuccess(JsonObject jsonObject) {

            view.hideProgressDialog();
            setProductStatus(ProductView.STATUS_CART);
            view.updateProductStatus();

            // TODO : Reload cart (Cart gets updated automatically. So keep this away for a while)
            // Do this by passing intent in result
            // If the user goes directly to the cart, pass arguments

            /*if (MainActivity.mCartFragment != null) {
                MainActivity.mCartFragment.mPresenter.loadData(true);
            }*/
            ToastUtils.showShortToast(resources.getString(R.string.add_to_cart_success));
        }

        @Override
        public void onFail(String errorMessage) {

            view.hideProgressDialog();
            setProductStatus(ProductActivity.STATUS_BUY);
            view.updateProductStatus();

            // TODO : Reload cart (Cart gets updated automatically. So keep this away for a while)
            // Do this by passing intent in result
            // If the user goes directly to the cart, pass arguments
                /*if (MainActivity.mCartFragment != null) {
                    MainActivity.mCartFragment.mPresenter.loadData(true);
                }*/
            ToastUtils.showShortToast(errorMessage);
        }
    }

    @Override
    public void reportProduct() {

        if (ConnectivityUtils.isConnectedToInternet()) {

            view.showProgressDialog(resources.getString(R.string.loading_please_wait));
            reportProductApiCall = apiProvider.reportProduct(
                    getPostId(), getUserAuth(), new ReportProductApiListener());
        } else {
            ToastUtils.showShortToast(resources.getString(R.string.internet_error));
        }
    }

    private class ReportProductApiListener implements ApiProvider.ReportProductListener {

        @Override
        public void onSuccess(JsonObject jsonObject) {

            view.hideProgressDialog();
            view.showReportProductSuccessDialog();
        }

        @Override
        public void onFail(String errorMessage) {

            view.hideProgressDialog();
            view.showReportProductFailDialog();
        }
    }

    @Override
    public void addOrEditProduct(float secondsToPreview, int pictureCount, boolean isBeingAdded,
            String addressId, ProductItem productItem) {

        if (ConnectivityUtils.isConnectedToInternet()) {

            String postId = productItem.getPostId();
            String title = productItem.getTitle();
            String description = productItem.getDescription();
            String sellingPrice = String.valueOf(productItem.getSellingPrice());
            String purchasePrice = String.valueOf(productItem.getPurchasePrice());
            String brandId = productItem.getBrandId();
            String brandName = productItem.getBrandName();
            String colorId = productItem.getColorId();
            String sizeId = productItem.getSizeId();
            String newWithTags = productItem.isNewWithTags() ? "1" : "0";
            String categoryId = productItem.getCategoryId();

            view.showProgressDialog(resources.getString(R.string.loading_please_wait));
            addOrEditProductApiCall = apiProvider.addOrEditPost(
                    isBeingAdded, postId, title, description, sellingPrice, purchasePrice, brandId,
                    brandName, colorId, sizeId, newWithTags, categoryId, addressId, getUserAuth(),
                    new AddOrEditProductApiListener());
        } else {
            ToastUtils.showShortToast(resources.getString(R.string.internet_error));
        }
    }

    private class AddOrEditProductApiListener implements ApiProvider
            .AddOrEditProductListener {

        @Override
        public void onSuccess(boolean isBeingAdded, JsonObject jsonObject) {

            view.hideProgressDialog();

            if (isBeingAdded) {

                boolean isNumberVerified = true;
                String number = "";

                try {

                    Gson gson = new Gson();
                    String JSONString = gson.toJson(jsonObject);
                    JSONObject jsonResponse = new JSONObject(JSONString);
                    JSONObject joContent = jsonResponse.getJSONObject(ApiResponse.KEY_CONTENT);

                    if (joContent.has(ApiResponse.KEY_PHONE_NUMBER)) {
                        number = joContent.getString(ApiResponse.KEY_PHONE_NUMBER);
                    }
                    if (joContent.has(ApiResponse.KEY_IS_VERIFIED_PHONE)) {
                        isNumberVerified = joContent.getString(ApiResponse.KEY_IS_VERIFIED_PHONE)
                                                    .equals("1");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                view.showAddProductSuccessDialog(number, isNumberVerified);
            } else {

                ToastUtils.showShortToast(resources.getString(R.string.edit_product_success));
                view.sendResultBackToCallingActivity(Activity.RESULT_OK);
            }
        }

        @Override
        public void onFail(boolean isBeingAdded, String errorMessage) {

            view.hideProgressDialog();
            ToastUtils.showShortToast(errorMessage);
            view.sendResultBackToCallingActivity(ProductView.RESULT_ADD_EDIT_POST_FAIL);
        }
    }

    @Override
    public void toggleFollow() {

        if (ConnectivityUtils.isConnectedToInternet()) {

            if (handler == null || followRunnable == null) {
                Log.e(TAG, "handler or runnable is null");
                return;
            }

            handler.removeCallbacks(followRunnable);
            handler.postDelayed(followRunnable, FOLLOW_WAIT);

            setIsSellerFollowed(!isSellerFollowed());
            view.showFollowStatus(isSellerFollowed());
        } else {
            ToastUtils.showShortToast(resources.getString(R.string.internet_error));
        }
    }

    private class ToggleUserFollowApiListener implements ApiProvider
            .ToggleUserFollowListener {

        @Override
        public void onSuccess(boolean toFollow, JsonObject jsonObject) {


            //Update Style feed
            // TODO Change this. Reload style only when the user goes there.
//            EventBus.getDefault().post(new MainActivityUpdateEvent(
//                    MainActivityUpdateEvent.EVENT_UPDATE_STYLE_FEED));
        }

        @Override
        public void onFail(boolean toFollow, String errorMessage) {
            view.showFollowStatus(!toFollow);
        }
    }

    @Override
    public void toggleLike() {

        if (isLikeBeingToggled()) {
            Log.e(TAG, "like is being toggled");
            return;
        }

        if (ConnectivityUtils.isConnectedToInternet()) {

            if (handler == null || likeRunnable == null) {
                Log.e(TAG, "handler or runnable is null");
                return;
            }

            handler.removeCallbacks(likeRunnable);
            handler.postDelayed(likeRunnable, LIKE_WAIT);

            setIsLiked(!isLiked());
            view.updateLikeStatus();
            setLikesCount(isLiked() ? getLikesCount() + 1 : getLikesCount() - 1);
            view.updateLikesCount();
        } else {
            ToastUtils.showShortToast(resources.getString(R.string.internet_error));
        }
    }

    private class ToggleProductLikeApiListener implements ApiProvider
            .ToggleProductLikeListener {

        @Override
        public void onSuccess(boolean isToBeLiked, JsonObject jsonObject) {

            setIsLikeBeingToggled(false);
            if (ProductView.SOURCE_PROFILE.equals(getSource())) {

                // TODO : Reload data in ProfileFragment. This is unnecessary.
                // It's okay to have some stale data.
                // ProfileFragment.mShouldReloadData = true;
            }
        }

        @Override
        public void onFail(boolean isToBeLiked, String errorMessage) {

            setIsLikeBeingToggled(false);
            setIsLiked(!isToBeLiked);
            view.updateLikeStatus();
            setLikesCount(isToBeLiked ? getLikesCount() - 1 : getLikesCount() + 1);
            view.updateLikesCount();
        }
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setSecondsToPreview(float secondsToPreview) {
        this.secondsToPreview = secondsToPreview;
    }

    public Float getSecondsToPreview() {
        return secondsToPreview;
    }

    public void setConfirmStartTime(long confirmStartTime) {
        this.confirmStartTime = confirmStartTime;
    }

    public long getConfirmStartTime() {
        return confirmStartTime;
    }

    @Override
    public void setUserAuth(UserAuth userAuth) {
        this.userAuth = userAuth;
    }

    @Override
    public UserAuth getUserAuth() {
        return userAuth;
    }

    public void setProductItem(ProductItem productItem) {
        this.productItem = productItem;
    }

    public ProductItem getProductItem() {
        return productItem;
    }

    @Override
    public void setPostId(String postId) {
        this.postId = postId;
    }

    @Override
    public String getPostId() {
        return postId;
    }

    public void setIsFeatured(boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    public boolean isFeatured() {
        return isFeatured;
    }

    public void setPictureCount(int pictureCount) {
        this.pictureCount = pictureCount;
    }

    public int getPictureCount() {
        return pictureCount;
    }

    public boolean isBeingAdded() {
        return isBeingAdded;
    }

    public void setIsBeingAdded(boolean isBeingAdded) {
        this.isBeingAdded = isBeingAdded;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public void setSellerUserId(String sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getSellerUserId() {
        return sellerUserId;
    }

    @Override
    public void setSellerUserName(String sellerUserName) {
        this.sellerUserName = sellerUserName;
    }

    @Override
    public String getSellerUserName() {
        return sellerUserName;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsProductDetailsLoaded(boolean isProductDetailsLoaded) {
        this.isProductDetailsLoaded = isProductDetailsLoaded;
    }

    public boolean isProductDetailsLoaded() {
        return isProductDetailsLoaded;
    }

    public void setCommentsList(
            ArrayList<CommentItem> commentsList) {
        this.commentsList = commentsList;
    }

    public ArrayList<CommentItem> getCommentsList() {
        return commentsList;
    }

    @Override
    public String getProductStatus() {
        return productStatus;
    }

    @Override
    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public boolean isSellerFollowed() {
        return isSellerFollowed;
    }

    public void setIsSellerFollowed(boolean isSellerFollowed) {
        this.isSellerFollowed = isSellerFollowed;
    }

    public boolean isLikeBeingToggled() {
        return isLikeBeingToggled;
    }

    public void setIsLikeBeingToggled(boolean isLikeBeingToggled) {
        this.isLikeBeingToggled = isLikeBeingToggled;
    }

    public ArrayList<ProductImageItem> getProductImagesList() {
        return productImagesList;
    }

    public void setProductImagesList(ArrayList<ProductImageItem> productImagesList) {
        this.productImagesList = productImagesList;
    }
}
