package com.kabir.example.main;

public class ProductImageItem {

    private String thumbnailUrl;
    private String mediumSizeUrl;
    private String largeSizeUrl;
    private boolean isSelected;
    private boolean isLoaded;

    /**
     * @param thumbnailUrl  The URL of the thumbnail image.
     * @param mediumSizeUrl The URL of the medium size image.
     * @param largeSizeUrl The URL of the large size image.
     */
    public ProductImageItem(String thumbnailUrl, String mediumSizeUrl, String largeSizeUrl) {

        this.thumbnailUrl = thumbnailUrl;
        this.mediumSizeUrl = mediumSizeUrl;
        this.largeSizeUrl = largeSizeUrl;
        isSelected = false;
        isLoaded = false;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getMediumSizeUrl() {
        return mediumSizeUrl;
    }

    public String getLargeSizeUrl() {
        return largeSizeUrl;
    }

    public Boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Boolean isLoaded() {
        return isLoaded;
    }

    public void setIsLoaded(boolean isLoaded) {
        this.isLoaded = isLoaded;
    }
}
