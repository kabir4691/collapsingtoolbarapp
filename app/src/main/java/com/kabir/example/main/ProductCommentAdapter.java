package com.kabir.example.main;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kabir.example.utils.CommentItem;
import com.kabir.example.utils.CustomTypefaceSpan;
import com.kabir.example.utils.CustomURLSpan;
import com.kabir.example.R;
import com.kabir.example.utils.TypefaceUtils;

import java.util.ArrayList;
import java.util.regex.Matcher;

public class ProductCommentAdapter extends BaseAdapter {

    String TAG = this.getClass().getSimpleName();

    LayoutInflater mInflater;
    Activity mActivity;
    ArrayList<CommentItem> mList;
    Holder mHolder;
    TextAppearanceSpan mUsernameAppearanceSpan, mCommentAppearanceSpan;
    CustomTypefaceSpan mUsernameTypefaceSpan, mCommentTypefaceSpan;

    public ProductCommentAdapter(Activity activity, ArrayList<CommentItem> list) {

        mActivity = activity;
        mList = list;
        mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mUsernameAppearanceSpan = new TextAppearanceSpan(mActivity,
                                                         R.style.ProductCommentsUsernameStyle);
        mUsernameTypefaceSpan = new CustomTypefaceSpan(null,
                                                       TypefaceUtils.getLato700Typeface(mActivity));
        mCommentAppearanceSpan = new TextAppearanceSpan(mActivity,
                                                        R.style.ProductCommentsCommentStyle);
        mCommentTypefaceSpan = new CustomTypefaceSpan(null,
                                                      TypefaceUtils.getLato400Typeface(mActivity));
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class Holder {
        TextView tv_text;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            mHolder = new Holder();

            convertView = mInflater.inflate(R.layout.item_product_lv_comments, null);
            mHolder.tv_text = (TextView) convertView.findViewById(R.id.tv_text);
            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }

        String username = Html.fromHtml(mList.get(position).getUserName()).toString().concat(" ");
        String comment = Html.fromHtml(mList.get(position).getComment()).toString();

        mHolder.tv_text.setText(buildComment(username, comment));
        mHolder.tv_text.setMovementMethod(LinkMovementMethod.getInstance());

        return convertView;
    }

    /**
     * @param username The username of the commenter.
     * @param comment  The comment.
     * @return CharSequence containing the username and comment in its desired appearance.
     */
    CharSequence buildComment(String username, String comment) {

        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (username == null || username.isEmpty() || comment == null || comment.isEmpty()) {
            Log.e(TAG, "username or comment is null or empty");
        } else {

            builder.append(username.concat(" "));
            builder.setSpan(
                    mUsernameAppearanceSpan,
                    builder.length() - (username.concat(" ")).length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(
                    mUsernameTypefaceSpan,
                    builder.length() - (username.concat(" ")).length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            builder.append(comment);
            builder.setSpan(
                    mCommentAppearanceSpan,
                    builder.length() - comment.length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(
                    mCommentTypefaceSpan,
                    builder.length() - comment.length(),
                    builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            Matcher matcher = Patterns.WEB_URL.matcher(comment);
            while (matcher.find()) {

                int commentStart = builder.length() - comment.length();
                int urlStart = matcher.start();
                int urlEnd = matcher.end();

                String url = comment.subSequence(urlStart, urlEnd).toString();

                CustomURLSpan urlSpan = new CustomURLSpan(url);

                builder.setSpan(
                        urlSpan, commentStart + urlStart, commentStart + urlEnd,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        return builder;
    }
}