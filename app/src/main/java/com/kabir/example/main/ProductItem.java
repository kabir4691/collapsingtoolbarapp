package com.kabir.example.main;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductItem implements Parcelable{

    private String postId;
    private String sellerUserId;
    private String sellerUserName;
    private String sellerProfileImageUrl;
    private String coverPhotoUrl;
    private String otherPhoto1Url;
    private String otherPhoto2Url;
    private String otherPhoto3Url;
    private String otherPhoto4Url;
    private String title;
    private String description;
    private String categoryId;
    private String categoryName;
    private String brandId;
    private String brandName;
    private String sizeId;
    private String sizeName;
    private String colorId;
    private String colorName;
    private boolean isNewWithTags;
    private int sellingPrice;
    private int purchasePrice;

    public ProductItem () {
    }

    protected ProductItem(Parcel in) {
        postId = in.readString();
        sellerUserId = in.readString();
        sellerUserName = in.readString();
        sellerProfileImageUrl = in.readString();
        coverPhotoUrl = in.readString();
        otherPhoto1Url = in.readString();
        otherPhoto2Url = in.readString();
        otherPhoto3Url = in.readString();
        otherPhoto4Url = in.readString();
        title = in.readString();
        description = in.readString();
        categoryId = in.readString();
        categoryName = in.readString();
        brandId = in.readString();
        brandName = in.readString();
        sizeId = in.readString();
        sizeName = in.readString();
        colorId = in.readString();
        colorName = in.readString();
        isNewWithTags = in.readByte() != 0;
        sellingPrice = in.readInt();
        purchasePrice = in.readInt();
    }

    public static final Creator<ProductItem> CREATOR = new Creator<ProductItem>() {
        @Override
        public ProductItem createFromParcel(Parcel in) {
            return new ProductItem(in);
        }

        @Override
        public ProductItem[] newArray(int size) {
            return new ProductItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(postId);
        dest.writeString(sellerUserId);
        dest.writeString(sellerUserName);
        dest.writeString(sellerProfileImageUrl);
        dest.writeString(coverPhotoUrl);
        dest.writeString(otherPhoto1Url);
        dest.writeString(otherPhoto2Url);
        dest.writeString(otherPhoto3Url);
        dest.writeString(otherPhoto4Url);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(categoryId);
        dest.writeString(categoryName);
        dest.writeString(brandId);
        dest.writeString(brandName);
        dest.writeString(sizeId);
        dest.writeString(sizeName);
        dest.writeString(colorId);
        dest.writeString(colorName);
        dest.writeByte((byte) (isNewWithTags ? 1 : 0));
        dest.writeInt(sellingPrice);
        dest.writeInt(purchasePrice);
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(String sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getSellerUserName() {
        return sellerUserName;
    }

    public void setSellerUserName(String sellerUserName) {
        this.sellerUserName = sellerUserName;
    }

    public String getSellerProfileImageUrl() {
        return sellerProfileImageUrl;
    }

    public void setSellerProfileImageUrl(String sellerProfileImageUrl) {
        this.sellerProfileImageUrl = sellerProfileImageUrl;
    }

    public String getCoverPhotoUrl() {
        return coverPhotoUrl;
    }

    public void setCoverPhotoUrl(String coverPhotoUrl) {
        this.coverPhotoUrl = coverPhotoUrl;
    }

    public String getOtherPhoto1Url() {
        return otherPhoto1Url;
    }

    public void setOtherPhoto1Url(String otherPhoto1Url) {
        this.otherPhoto1Url = otherPhoto1Url;
    }

    public String getOtherPhoto2Url() {
        return otherPhoto2Url;
    }

    public void setOtherPhoto2Url(String otherPhoto2Url) {
        this.otherPhoto2Url = otherPhoto2Url;
    }

    public String getOtherPhoto3Url() {
        return otherPhoto3Url;
    }

    public void setOtherPhoto3Url(String otherPhoto3Url) {
        this.otherPhoto3Url = otherPhoto3Url;
    }

    public String getOtherPhoto4Url() {
        return otherPhoto4Url;
    }

    public void setOtherPhoto4Url(String otherPhoto4Url) {
        this.otherPhoto4Url = otherPhoto4Url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public boolean isNewWithTags() {
        return isNewWithTags;
    }

    public void setIsNewWithTags(boolean isNewWithTags) {
        this.isNewWithTags = isNewWithTags;
    }

    public int getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(int sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(int purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
}
