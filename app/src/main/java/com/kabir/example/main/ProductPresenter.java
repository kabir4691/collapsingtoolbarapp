package com.kabir.example.main;

import android.os.Bundle;

import com.kabir.example.utils.CommentItem;
import com.kabir.example.utils.UserAuth;

import java.util.ArrayList;

public interface ProductPresenter {

    boolean attachView(Bundle extras);
    void detachView();

    void loadData();

    void loadProductData();
    void loadSellerData();
    void loadCommentsData();
    void reportProduct();

    CharSequence buildDescription(String username, String description);

    void addOrEditProduct(float secondsToPreview, int pictureCount, boolean isBeingAdded,
            String addressId, ProductItem productItem);

    void addToCart();
    void toggleFollow();
    void toggleLike();

    void setUserAuth(UserAuth userAuth);
    UserAuth getUserAuth();

    void setProductItem(ProductItem productItem);
    ProductItem getProductItem();

    void setSource(String source);
    String getSource();

    void setPostId(String postId);
    String getPostId();

    void setProductStatus(String status);
    String getProductStatus();

    void setSecondsToPreview(float seconds);
    Float getSecondsToPreview();

    void setPictureCount(int pictureCount);
    int getPictureCount();

    void setIsBeingAdded(boolean isBeingAdded);
    boolean isBeingAdded();

    void setAddressId(String addressId);
    String getAddressId();

    void setSellerUserId(String userId);
    String getSellerUserId();

    void setSellerUserName(String username);
    String getSellerUserName();

    void setCommentsList(ArrayList<CommentItem> list);
    ArrayList<CommentItem> getCommentsList();

    void setLikesCount(int likesCount);
    int getLikesCount();

    void setIsLiked(boolean isLiked);
    boolean isLiked();

    void setIsProductDetailsLoaded(boolean isProductDetailsLoaded);
    boolean isProductDetailsLoaded();

    void setIsSellerFollowed(boolean isSellerFollowed);
    boolean isSellerFollowed();

    void setIsFeatured(boolean isFeatured);
    boolean isFeatured();

    void setProductImagesList(ArrayList<ProductImageItem> list);
    ArrayList<ProductImageItem> getProductImagesList();
}
