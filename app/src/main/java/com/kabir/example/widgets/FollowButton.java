package com.kabir.example.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kabir.example.R;

/**
 * Created by jay on 31/8/15.
 */
public class FollowButton extends LinearLayout {

    private Drawable mActiveDrawable;
    private Drawable mInactiveDrawable;

    private TextView mTextView;
    private ImageView mImageView;

    private ColorStateList mActiveTextColor;
    private ColorStateList mInactiveTextColor;

    private Drawable mActiveImageDrawable;
    private Drawable mInactiveImageDrawable;

    private boolean isFollowing = false;

    private String followingText;
    private String followText;

    public FollowButton(Context context) {
        super(context);
        init(context);
    }

    public FollowButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FollowButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

        mActiveDrawable = ContextCompat
                .getDrawable(context, R.drawable.follow_button_background_theme);
        mInactiveDrawable = ContextCompat
                .getDrawable(context, R.drawable.follow_button_background_grey);
        mActiveTextColor = context.getResources().getColorStateList(R.color.follow_button_active_text_color);
        mInactiveTextColor = context.getResources().getColorStateList(
                R.color.follow_button_inactive_text_color);
        mActiveImageDrawable = ContextCompat.getDrawable(context, R.drawable.ic_plus_white_12dp);
        mInactiveImageDrawable = ContextCompat
                .getDrawable(context, R.drawable.ic_choose_light_grey_12dp);
        followingText = "Following";
        followText = "Follow";

        LayoutInflater mInflater = LayoutInflater.from(context);
        mInflater.inflate(R.layout.follow_button_layout, this, true);

        mTextView = (TextView) findViewById(R.id.textview);
        mImageView = (ImageView) findViewById(R.id.imageview);
    }

    public void follow() {

        mTextView.setText(followingText);
        mTextView.setTextColor(mInactiveTextColor);
        setBackground(mInactiveDrawable);
        mImageView.setImageDrawable(mInactiveImageDrawable);
        isFollowing = true;
    }

    public void unfollow() {

        mTextView.setText(followText);
        mTextView.setTextColor(mActiveTextColor);
        setBackground(mActiveDrawable);
        mImageView.setImageDrawable(mActiveImageDrawable);
        isFollowing = false;
    }

    public boolean isFollowing() {
        return isFollowing;
    }
}
