package com.kabir.example.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ToggleSwipeViewPager extends ViewPager {

    boolean canSwipe = true;

    public ToggleSwipeViewPager(Context context) {
        super(context);
    }

    public ToggleSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        if (canSwipe) {
            return  super.onInterceptTouchEvent(event);
        } else {
        return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (canSwipe) {
            return  super.onTouchEvent(event);
        } else {
            return false;
        }
    }

    public void setSwipeable(boolean canSwipe) {
        this.canSwipe = canSwipe;
    }
}
