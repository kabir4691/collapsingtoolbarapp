package com.kabir.example.widgets;

import android.content.Context;
import android.util.AttributeSet;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class CustomImageViewTouch extends ImageViewTouch {

    final String TAG = "CustomImageViewTouch";

    Callback mCallback;

    public CustomImageViewTouch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomImageViewTouch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onZoom(float scale) {
        super.onZoom(scale);

        if (mCallback != null) {
            mCallback.onZoom(scale);
        }
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {

        void onZoom(float scale);
    }
}
