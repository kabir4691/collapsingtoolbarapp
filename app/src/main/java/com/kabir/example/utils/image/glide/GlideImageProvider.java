package com.kabir.example.utils.image.glide;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.kabir.example.R;
import com.kabir.example.utils.image.ImageProvider;

public class GlideImageProvider implements ImageProvider {

    private Glide glide;
    private RequestManager requestManager;
    private int placeHolderResourceId = R.drawable.image_placeholder_square;
    private int errorResourceId = R.drawable.image_placeholder_square;

    public GlideImageProvider(Activity activity) {
        glide = Glide.get(activity.getApplicationContext());
        requestManager = Glide.with(activity);

    }

    public GlideImageProvider(Fragment fragment) {
        glide = Glide.get(fragment.getActivity().getApplicationContext());
        requestManager = Glide.with(fragment);
    }

    @Override
    public void displayImage(String url, ImageView imageView) {

        DrawableRequestBuilder requestBuilder = requestManager.load(url);
        requestBuilder = setDrawableSettings(requestBuilder);
        requestBuilder.into(imageView);
    }

    @Override
    public void displayImage(String url, int width, int height, final ImageView imageView) {

        BitmapRequestBuilder requestBuilder = requestManager.load(url).asBitmap();
        requestBuilder = setBitmapSettings(requestBuilder);
        requestBuilder.into(new SimpleTarget<Bitmap>(width, height) {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                imageView.setImageBitmap(bitmap);
            }
        });
    }

    private DrawableRequestBuilder setDrawableSettings(DrawableRequestBuilder builder) {

        if (placeHolderResourceId != -1) {
            builder.placeholder(placeHolderResourceId);
        }
        if (errorResourceId != -1) {
            builder.error(errorResourceId);
        }
        return builder;
    }
    private BitmapRequestBuilder setBitmapSettings(BitmapRequestBuilder builder) {

        if (placeHolderResourceId != -1) {
            builder.placeholder(placeHolderResourceId);
        }
        if (errorResourceId != -1) {
            builder.error(errorResourceId);
        }
        return builder;
    }



    @Override
    public void setPlaceHolderResourceId(int placeHolderResourceId) {
        this.placeHolderResourceId = placeHolderResourceId;
    }

    @Override
    public void setErrorResourceId(int errorResourceId) {
        this.errorResourceId = errorResourceId;
    }

    public void clearMemory() {
        glide.clearMemory();
        System.gc();
    }
}