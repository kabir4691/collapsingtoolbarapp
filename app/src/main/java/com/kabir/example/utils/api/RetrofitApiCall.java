package com.kabir.example.utils.api;

import com.google.gson.JsonObject;
import com.kabir.example.utils.api.ApiCall;

import retrofit.Call;

/**
 * Created by Kabir on 27-11-2015.
 */
public class RetrofitApiCall implements ApiCall {

    Call<JsonObject> call;

    public RetrofitApiCall(Call<JsonObject> call) {
        this.call = call;
    }

    @Override
    public void cancel() {
        call.cancel();
    }
}
