package com.kabir.example.utils;

public class AppConfig {

    /**
     * The format of timestamp obtained from the server.
     */
    public static final String FORMAT_TIMESTAMP = "yyyy-MM-dd kk:mm:ss";

    /**
     * The name of the folder in the external storage directory where images created by our app
     * are stored.
     */
    public static final String NAME_FOLDER_IMAGES = "Elanic";

    /**
     * The name (prefix) of the images created by our app.
     */
    public static final String NAME_FILE_IMAGES = "Photo_";

    /**
     * The maximum file size (in bytes) of the image whose Base64 string is sent
     * to the server.
     */
    public static final double SIZE_MAXIMUM_IMAGE_FILE = 1024 * 1024;

    /**
     * The maximum width or height (in pixels) of the image bitmap whose Base64
     * string is sent to the server.
     */
    public static final int SIZE_MAXIMUM_IMAGE_BITMAP = 1000;

    public static final String HEADER_BASE64 = "data:image/jpeg;base64,";
}
