package com.kabir.example.utils;

import android.content.ActivityNotFoundException;
import android.text.style.URLSpan;
import android.view.View;

public class CustomURLSpan extends URLSpan {

    public CustomURLSpan(String url) {
        super(url);
    }

    @Override
    public void onClick(View widget) {

        try {
            super.onClick(widget);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
