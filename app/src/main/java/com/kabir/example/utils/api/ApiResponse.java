package com.kabir.example.utils.api;

public class ApiResponse {

    public static final String KEY_STATUS = "status";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_CONTENT = "content";
    public static final String KEY_ID = "id";
    public static final String KEY_THUMBNAIL = "thumbnail";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PICTURE = "picture";
    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String KEY_POST_STATUS = "post_status";
    public static final String KEY_TITLE = "title";
    public static final String KEY_BRAND = "brand";
    public static final String KEY_COLOR = "color";
    public static final String KEY_SIZE = "size";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_PRICE = "price";
    public static final String KEY_PURCHASE_PRICE = "purchase_price";
    public static final String KEY_AVAILABLE = "available";
    public static final String KEY_LIKES = "likes";
    public static final String KEY_IS_LIKED = "isLiked";
    public static final String KEY_PICTURE_0 = "picture_0";
    public static final String KEY_PICTURE_1 = "picture_1";
    public static final String KEY_PICTURE_2 = "picture_2";
    public static final String KEY_PICTURE_3 = "picture_3";
    public static final String KEY_PICTURE_4 = "picture_4";
    public static final String KEY_MEDIUM_SIZE = "medium_size";
    public static final String KEY_LARGE_SIZE = "large_size";
    public static final String KEY_PHONE_NUMBER = "phone_number";
    public static final String KEY_IS_VERIFIED_PHONE = "is_verified_phone";
    public static final String KEY_IS_FOLLOWING = "isFollowing";
    public static final String KEY_CATEGORY_NAME = "category_name";
    public static final String KEY_TEXT = "text";
    public static final String KEY_NWT = "nwt";
    public static final String KEY_ISCART = "isCart";

    public static final String VALUE_STATUS_SUCCESS = "success";

    public static final String VALUE_POST_STATUS_APPROVED = "approved";
    public static final String VALUE_POST_STATUS_REJECTED = "rejected";
    public static final String VALUE_POST_STATUS_SOLD = "sold";
    public static final String VALUE_POST_STATUS_CANCELLED = "cancelled";
    public static final String VALUE_POST_STATUS_DELETED = "deleted";
    public static final String VALUE_POST_STATUS_PENDING = "pending";
    public static final String VALUE_POST_STATUS_ONHOLD = "onhold";
    public static final String VALUE_POST_STATUS_INCOMPLETE = "incomplete";
    public static final String VALUE_POST_STATUS_UNAPPROVED = "unapproved";
}