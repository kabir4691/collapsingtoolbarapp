package com.kabir.example.utils;

import android.widget.Toast;

public class ToastUtils {

    /**
     * Show a toast for 2 seconds.
     *
     * @param message The message to display.
     */
    public static void showShortToast(CharSequence message) {
        Toast.makeText(AppContext.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show a toast for 3.5 seconds.
     *
     * @param message The message to display.
     */
    public static void showLongToast(String message) {
        Toast.makeText(AppContext.getInstance(), message, Toast.LENGTH_LONG).show();
    }
}
