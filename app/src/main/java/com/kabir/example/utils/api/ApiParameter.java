package com.kabir.example.utils.api;

/**
 * Created by Kabir on 27-11-2015.
 */
public class ApiParameter {

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USER_ID2 = "user_id2";
    public static final String KEY_LOGIN_KEY = "login_key";
    public static final String KEY_POST_ID = "post_id";
    public static final String KEY_ADDRESS_ID = "address_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_BRAND_NAME = "brand_name";
    public static final String KEY_BRAND_ID = "brand_id";
    public static final String KEY_SIZE_ID = "size_id";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_COLOR_ID = "color_id";
    public static final String KEY_PRICE = "price";
    public static final String KEY_PURCHASE_PRICE = "purchase_price";
    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_NWT = "nwt";
}
