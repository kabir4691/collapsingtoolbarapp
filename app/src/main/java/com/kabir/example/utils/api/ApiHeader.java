package com.kabir.example.utils.api;

public class ApiHeader {

    public static final String KEY_PLATFORM = "Platform";
    public static final String KEY_SYSTEM_VERSION = "SystemVersion";
    public static final String KEY_APP_VERSION = "AppVersion";

    public static final String VALUE_PLATFORM_ANDROID = "android";
}
