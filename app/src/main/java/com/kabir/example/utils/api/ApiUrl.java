package com.kabir.example.utils.api;

public class ApiUrl {

    public static final String BASE = "http://54.169.71.129";

    public static final String ENDPOINT_PRODUCT_LIKE = "/api/dev/android/post/like";
    public static final String ENDPOINT_PRODUCT_UNLIKE = "/api/dev/android/post/unlike";
    public static final String ENDPOINT_PRODUCT_DETAILS = "/api/dev/android/post";
    public static final String ENDPOINT_PRODUCT_REPORT = "/api/dev/android/report_abuse";
    public static final String ENDPOINT_CART_ADD = "/api/dev/android/cart/add";
    public static final String ENDPOINT_PROFILE_DETAILS = "/api/dev/android/profile";
    public static final String ENDPOINT_PROFILE_FOLLOW = "/api/dev/android/profile/follow";
    public static final String ENDPOINT_PROFILE_UNFOLLOW = "/api/dev/android/profile/unfollow";
    public static final String ENDPOINT_PRODUCT_COMMENTS = "/api/dev/android/post/comment";
    public static final String ENDPOINT_SELL_POST_ADD = "/api/dev/android/post/add/details";
    public static final String ENDPOINT_SELL_POST_EDIT = "/api/dev/android/post/edit/details";

}