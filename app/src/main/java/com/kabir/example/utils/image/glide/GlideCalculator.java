package com.kabir.example.utils.image.glide;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

import com.bumptech.glide.load.DecodeFormat;

/**
 * A calculator that tries to intelligently determine cache sizes for a given device based on
 * some constants and the devices screen density, width, and height.
 */
public class GlideCalculator {

    private final String TAG = "GlideCalculator";

    private final int bitmapPoolSize;
    private final int memoryCacheSize;
    private final Context context;

    private final int MEMORY_CACHE_TARGET_SCREENS = 2;
    private final int BITMAP_POOL_TARGET_SCREENS = 4;
    private final int MAX_SIZE_PERCENTAGE = 40;
    private final int LOW_MEMORY_MAX_SIZE_PERCENTAGE = 25;

    interface ScreenDimensions {

        int getWidthPixels();

        int getHeightPixels();
    }

    public GlideCalculator(Context context, DecodeFormat format) {

        this(context, format, (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE),
             new DisplayMetricsScreenDimensions(context.getResources().getDisplayMetrics()));
    }

    GlideCalculator(Context context, DecodeFormat format, ActivityManager activityManager,
            ScreenDimensions screenDimensions) {

        this.context = context;
        final int maxSize = getMaxSize(activityManager);

        final int screenSize = screenDimensions.getWidthPixels() * screenDimensions
                .getHeightPixels() *
                getBytesPerPixel(format);

        int targetPoolSize = screenSize * BITMAP_POOL_TARGET_SCREENS;
        int targetMemoryCacheSize = screenSize * MEMORY_CACHE_TARGET_SCREENS;

        if (targetMemoryCacheSize + targetPoolSize <= maxSize) {

            memoryCacheSize = targetMemoryCacheSize;
            bitmapPoolSize = targetPoolSize;
        } else {

            int part = Math.round(
                    (float) maxSize / (BITMAP_POOL_TARGET_SCREENS + MEMORY_CACHE_TARGET_SCREENS));
            memoryCacheSize = part * MEMORY_CACHE_TARGET_SCREENS;
            bitmapPoolSize = part * BITMAP_POOL_TARGET_SCREENS;
        }

        Log.wtf(TAG,
                "target memory cache size: " + toMb(
                        targetMemoryCacheSize) + "final memory cache size: " +
                        toMb(memoryCacheSize) + "target pool size: " + toMb(targetPoolSize)
                        + "final pool size: " + toMb(bitmapPoolSize)
                        + " memory class limited: "
                        + (targetMemoryCacheSize + targetPoolSize > maxSize)
                        + " max size: " + toMb(maxSize) + " memoryClass: "
                        + activityManager.getMemoryClass() + " isLowMemoryDevice: "
                        + isLowMemoryDevice(activityManager));
    }

    /**
     * Returns the recommended memory cache size for the device it is run on in
     * bytes.
     */
    public int getMemoryCacheSize() {
        return memoryCacheSize;
    }

    /**
     * Returns the recommended bitmap pool size for the device it is run on in
     * bytes.
     */
    public int getBitmapPoolSize() {
        return bitmapPoolSize;
    }

    private int getMaxSize(ActivityManager activityManager) {
        final int memoryClassBytes = activityManager.getMemoryClass() * 1024 * 1024;
        final boolean isLowMemoryDevice = isLowMemoryDevice(activityManager);
        return Math.round(
                (memoryClassBytes * (isLowMemoryDevice ? LOW_MEMORY_MAX_SIZE_PERCENTAGE :
                        MAX_SIZE_PERCENTAGE)) / 100);
    }

    private String toMb(int bytes) {
        return Formatter.formatFileSize(context, bytes);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private boolean isLowMemoryDevice(ActivityManager activityManager) {

        final int sdkInt = Build.VERSION.SDK_INT;
        return sdkInt < Build.VERSION_CODES.HONEYCOMB || (sdkInt >= Build.VERSION_CODES.KITKAT &&
                activityManager.isLowRamDevice());
    }

    private int getBytesPerPixel(DecodeFormat format) {
        return (format.equals(DecodeFormat.PREFER_RGB_565)) ? 2 : 4;
    }

    private static class DisplayMetricsScreenDimensions implements ScreenDimensions {

        private final DisplayMetrics displayMetrics;

        public DisplayMetricsScreenDimensions(DisplayMetrics displayMetrics) {
            this.displayMetrics = displayMetrics;
        }

        @Override
        public int getWidthPixels() {
            return displayMetrics.widthPixels;
        }

        @Override
        public int getHeightPixels() {
            return displayMetrics.heightPixels;
        }
    }
}