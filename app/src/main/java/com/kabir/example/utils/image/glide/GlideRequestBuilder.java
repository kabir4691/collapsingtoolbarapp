package com.kabir.example.utils.image.glide;

import com.bumptech.glide.DrawableRequestBuilder;
import com.kabir.example.utils.image.RequestBuilder;

/**
 * Created by Kabir on 27-11-2015.
 */
public class GlideRequestBuilder implements RequestBuilder {

    DrawableRequestBuilder requestBuilder;

    public GlideRequestBuilder(DrawableRequestBuilder requestBuilder) {
        this.requestBuilder = requestBuilder;
    }

    @Override
    public void setPlaceHolder(int resourceId) {
        requestBuilder.placeholder(resourceId);
    }

    @Override
    public void setError(int resourceId) {
        requestBuilder.error(resourceId);
    }
}
