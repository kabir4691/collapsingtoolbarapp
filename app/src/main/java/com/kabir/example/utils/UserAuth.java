package com.kabir.example.utils;

/**
 * Created by Kabir on 27-11-2015.
 */
public class UserAuth {

    private String userId;
    private String loginKey;

    public UserAuth(String userId, String loginKey) {
        this.userId = userId;
        this.loginKey = loginKey;
    }

    public String getUserId() {
        return userId;
    }

    public String getLoginKey() {
        return loginKey;
    }
}
