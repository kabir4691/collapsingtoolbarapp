package com.kabir.example.utils.api;

import com.google.gson.JsonObject;
import com.kabir.example.utils.UserAuth;

/**
 * Created by Kabir on 25-11-2015.
 */
public interface ApiProvider {

    ApiCall getProductDetails(String postId, UserAuth userAuth, ProductDetailsListener listener);

    ApiCall getUserDetails(String userId, UserAuth userAuth, UserDetailsListener listener);

    ApiCall getProductComments(String postId, UserAuth userAuth, ProductCommentsListener listener);

    ApiCall toggleUserFollow(boolean isToBeFollowed, String userId, UserAuth userAuth,
            ToggleUserFollowListener listener);

    ApiCall toggleProductLike(boolean isToBeLiked, String postId, UserAuth userAuth,
            ToggleProductLikeListener listener);

    ApiCall reportProduct(String postId, UserAuth userAuth, ReportProductListener listener);

    ApiCall addProductToCart(String postId, UserAuth userAuth, AddProductToCartListener listener);

    ApiCall addOrEditPost(boolean isBeingAdded, String postId, String title, String description,
            String price, String purchasePrice, String brandId, String brandName,
            String colorId, String sizeId, String newWithTags, String categoryId, String addressId,
            UserAuth userAuth, AddOrEditProductListener listener);

    public interface ProductDetailsListener {

        void onSuccess(JsonObject jsonObject);

        void onFail(String errorMessage);
    }

    public interface ToggleProductLikeListener {

        void onSuccess(boolean isToBeLiked, JsonObject responseBody);

        void onFail(boolean isToBeLiked, String errorMessage);
    }

    public interface ToggleUserFollowListener {

        void onSuccess(boolean isToBeFollowed, JsonObject jsonObject);

        void onFail(boolean isToBeFollowed, String errorMessage);
    }

    public interface AddOrEditProductListener {

        void onSuccess(boolean isBeingAdded, JsonObject jsonObject);

        void onFail(boolean isBeingAdded, String errorMessage);
    }

    public interface ReportProductListener {

        void onSuccess(JsonObject jsonObject);

        void onFail(String errorMessage);
    }

    public interface ProductCommentsListener {

        void onSuccess(JsonObject jsonObject);

        void onFail(String errorMessage);
    }

    public interface UserDetailsListener {

        void onSuccess(JsonObject jsonObject);

        void onFail(String errorMessage);
    }

    public interface AddProductToCartListener {

        void onSuccess(JsonObject jsonObject);

        void onFail(String errorMessage);
    }
}


