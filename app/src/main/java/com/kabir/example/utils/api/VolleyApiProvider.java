package com.kabir.example.utils.api;

import android.os.Build;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kabir.example.BuildConfig;
import com.kabir.example.utils.AppContext;
import com.kabir.example.utils.UserAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyApiProvider implements ApiProvider {

    private final String TAG = "ApiHandler";

    public static RequestQueue requestQueue;
    private static Gson gson;

    public VolleyApiProvider() {

    }

    private static RequestQueue getRequestQueue() {

        if (requestQueue == null) {
            requestQueue = com.android.volley.toolbox.Volley
                    .newRequestQueue(AppContext.getInstance());
        }
        gson = new GsonBuilder().create();
        return requestQueue;
    }

    private void cancelRequests(Object tag) {

        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    private <T> void addToRequestQueue(com.android.volley.Request<T> req) {
        getRequestQueue().add(req);
    }

    private <T> void addToRequestQueue(com.android.volley.Request<T> req, String tag) {

        req.setTag(tag);
        getRequestQueue().add(req);
    }

    private Map<String, String> getGlobalHeaders() {

        Map<String, String> headers = new HashMap<>();
        headers.put(ApiHeader.KEY_PLATFORM, ApiHeader.VALUE_PLATFORM_ANDROID);
        headers.put(ApiHeader.KEY_APP_VERSION, BuildConfig.VERSION_NAME);
        headers.put(ApiHeader.KEY_SYSTEM_VERSION, String.valueOf(Build.VERSION.SDK_INT));
        return headers;
    }

    private boolean isStatusSuccess(JSONObject jsonObject) {

        boolean hasStatus = jsonObject.has(ApiResponse.KEY_STATUS);
        if (hasStatus) {

            String statusString = jsonObject.optString(ApiResponse.KEY_STATUS, "");
            return statusString.equalsIgnoreCase(ApiResponse.VALUE_STATUS_SUCCESS);
        }
        return false;
    }

    @Override
    public ApiCall getProductDetails(final String postId, final UserAuth userAuth,
            final ProductDetailsListener listener) {

        final String url = ApiUrl.BASE + ApiUrl.ENDPOINT_PRODUCT_DETAILS;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));

                listener.onFail(error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_POST_ID, postId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);

        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall getUserDetails(final String userId, final UserAuth userAuth,
            final UserDetailsListener listener) {

        final String url = ApiUrl.BASE + ApiUrl.ENDPOINT_PROFILE_DETAILS;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_USER_ID2, userId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall getProductComments(final String postId, final UserAuth userAuth,
            final ProductCommentsListener listener) {

        final String url = ApiUrl.BASE + ApiUrl.ENDPOINT_PRODUCT_COMMENTS;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_POST_ID, postId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall addProductToCart(final String postId, final UserAuth userAuth,
            final AddProductToCartListener listener) {

        final String url = ApiUrl.BASE + ApiUrl.ENDPOINT_CART_ADD;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_POST_ID, postId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall reportProduct(final String postId, final UserAuth userAuth,
            final ReportProductListener listener) {

        final String url = ApiUrl.BASE + ApiUrl.ENDPOINT_PRODUCT_REPORT;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_POST_ID, postId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall addOrEditPost(final boolean isBeingAdded,
            final String postId, final String title, final String description, final String price,
            final String purchasePrice, final String brandId, final String brandName,
            final String colorId, final String sizeId, final String newWithTags,
            final String categoryId, final String addressId, final UserAuth userAuth,
            final AddOrEditProductListener listener) {

        final String url =
                isBeingAdded ? ApiUrl.BASE + ApiUrl.ENDPOINT_SELL_POST_ADD :
                        ApiUrl.BASE + ApiUrl.ENDPOINT_SELL_POST_EDIT;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(isBeingAdded, gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(isBeingAdded, response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(isBeingAdded, error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_POST_ID, postId);
                params.put(ApiParameter.KEY_TITLE, title);
                params.put(ApiParameter.KEY_DESCRIPTION, description);
                params.put(ApiParameter.KEY_PRICE, price);
                params.put(ApiParameter.KEY_PURCHASE_PRICE, purchasePrice);
                params.put(ApiParameter.KEY_BRAND_ID, brandId);
                params.put(ApiParameter.KEY_BRAND_NAME, brandName);
                params.put(ApiParameter.KEY_COLOR_ID, colorId);
                params.put(ApiParameter.KEY_SIZE_ID, sizeId);
                params.put(ApiParameter.KEY_NWT, newWithTags);
                params.put(ApiParameter.KEY_CATEGORY_ID, categoryId);
                params.put(ApiParameter.KEY_ADDRESS_ID, addressId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall toggleUserFollow(final boolean isToBeFollowed, final String userId,
            final UserAuth userAuth, final ToggleUserFollowListener listener) {

        final String url =
                isToBeFollowed ? ApiUrl.BASE + ApiUrl.ENDPOINT_PROFILE_FOLLOW :
                        ApiUrl.BASE + ApiUrl.ENDPOINT_PROFILE_UNFOLLOW;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(isToBeFollowed,
                                           gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(isToBeFollowed, response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(isToBeFollowed, error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_USER_ID2, userId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }

    @Override
    public ApiCall toggleProductLike(final boolean isToBeLiked, final String postId,
            final UserAuth userAuth, final ToggleProductLikeListener listener) {

        final String url =
                isToBeLiked ? ApiUrl.BASE + ApiUrl.ENDPOINT_PRODUCT_LIKE :
                        ApiUrl.BASE + ApiUrl.ENDPOINT_PRODUCT_UNLIKE;
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "url : ".concat(url));
                Log.d(TAG, "response : ".concat(response));

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (isStatusSuccess(jsonObject)) {
                        listener.onSuccess(isToBeLiked, gson.fromJson(response, JsonObject.class));
                    } else {
                        listener.onFail(isToBeLiked, response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "url : ".concat(url));
                Log.e(TAG, "error : ".concat(error.toString()));
                listener.onFail(isToBeLiked, error.toString());
            }
        };

        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put(ApiParameter.KEY_POST_ID, postId);
                params.put(ApiParameter.KEY_USER_ID, userAuth.getUserId());
                params.put(ApiParameter.KEY_LOGIN_KEY, userAuth.getLoginKey());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getGlobalHeaders();
            }
        };

        request.setShouldCache(false);
        addToRequestQueue(request);
        return new VolleyApiCall(request);
    }
}