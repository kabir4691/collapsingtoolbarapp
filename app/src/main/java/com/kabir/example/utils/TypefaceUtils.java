package com.kabir.example.utils;

import android.content.Context;
import android.graphics.Typeface;

public class TypefaceUtils {

    static Typeface mLato300Typeface, mLato400Typeface, mLato700Typeface, mLato900Typeface,
            mMuseo300Typeface, mMuseo500Typeface, mIoniconsTypeface, mElanicTypeface;

    public static Typeface getLato300Typeface(Context context) {

        if (mLato300Typeface == null) {
            mLato300Typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Lato_300.ttf");
        }
        return mLato300Typeface;
    }

    public static Typeface getLato400Typeface(Context context) {

        if (mLato400Typeface == null) {
            mLato400Typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Lato_400.ttf");
        }
        return mLato400Typeface;
    }

    public static Typeface getLato700Typeface(Context context) {

        if (mLato700Typeface == null) {
            mLato700Typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Lato_700.ttf");
        }
        return mLato700Typeface;
    }

    public static Typeface getLato900Typeface(Context context) {

        if (mLato900Typeface == null) {
            mLato900Typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Lato_900.ttf");
        }
        return mLato900Typeface;
    }

    public static Typeface getMuseo300Typeface(Context context) {

        if (mMuseo300Typeface == null) {
            mMuseo300Typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Museo_300.otf");
        }
        return mMuseo300Typeface;
    }

    public static Typeface getMuseo500Typeface(Context context) {

        if (mMuseo500Typeface == null) {
            mMuseo500Typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Museo_500.otf");
        }
        return mMuseo500Typeface;
    }

    public static Typeface getIoniconsTypeface(Context context) {

        if (mIoniconsTypeface == null) {
            mIoniconsTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Ionicons.ttf");
        }
        return mIoniconsTypeface;
    }

    public static Typeface getElanicTypeface(Context context) {

        if (mElanicTypeface == null) {
            mElanicTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Elanic.ttf");
        }
        return mElanicTypeface;
    }
}
