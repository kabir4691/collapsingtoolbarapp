package com.kabir.example.utils.image;

/**
 * Created by Kabir on 27-11-2015.
 */
public interface RequestBuilder {

    void setPlaceHolder(int resourceId);
    void setError(int resourceId);
}
