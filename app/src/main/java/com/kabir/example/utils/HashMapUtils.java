package com.kabir.example.utils;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Kabir on 25-11-2015.
 */
public class HashMapUtils {

    public static String getKeyValuePairs(HashMap<String, String> hashMap) {

        String s = "";
        Iterator it = hashMap.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            String value = hashMap.get(key);
            s = s.concat(key.concat(" ").concat(value).concat("\n"));
        }
        return s;
    }
}
