package com.kabir.example.utils.api;

import android.os.Build;
import android.util.Log;

import com.google.gson.JsonObject;
import com.kabir.example.BuildConfig;
import com.kabir.example.utils.UserAuth;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;

public class RetrofitApiProvider implements ApiProvider {

    private final String TAG = "RetrofitApiProvider";

    private retrofit.Retrofit retrofit;
    private RetrofitInterface retrofitInterface;

    public RetrofitApiProvider() {

    }

    private retrofit.Retrofit getRetrofit() {

        if (retrofit == null) {

            OkHttpClient httpClient = new OkHttpClient();
            httpClient.interceptors()
                      .add(new Interceptor() {
                          @Override
                          public Response intercept(
                                  Interceptor.Chain chain) throws IOException {

                              Request original = chain.request();
                              Request request = original.newBuilder()
                                                        .header(
                                                                ApiHeader.KEY_PLATFORM,
                                                                ApiHeader.VALUE_PLATFORM_ANDROID)
                                                        .header(ApiHeader.KEY_APP_VERSION,
                                                                BuildConfig.VERSION_NAME)
                                                        .header(ApiHeader.KEY_SYSTEM_VERSION,
                                                                String.valueOf(
                                                                        Build.VERSION.SDK_INT))
                                                        .method(original.method(), original.body())
                                                        .build();
                              return chain.proceed(request);
                          }
                      });

            retrofit = new retrofit.Retrofit.Builder()
                    .baseUrl(ApiUrl.BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }

    private RetrofitInterface getRetrofitInterface() {

        if (retrofitInterface == null) {
            retrofitInterface = getRetrofit().create(RetrofitInterface.class);
        }
        return retrofitInterface;
    }

    private boolean isStatusSuccess(JsonObject object) {

        try {

            boolean hasStatus = object.has(ApiResponse.KEY_STATUS);
            if (hasStatus) {

                String statusString = object.get(ApiResponse.KEY_STATUS).getAsString();
                return statusString.equalsIgnoreCase(ApiResponse.VALUE_STATUS_SUCCESS);
            }
        } catch (ClassCastException | IllegalStateException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public ApiCall getProductDetails(String postId, UserAuth userAuth,
            final ProductDetailsListener listener) {

        Log.i(TAG, "url : ".concat(ApiUrl.BASE).concat(ApiUrl.ENDPOINT_PRODUCT_DETAILS));
        Call<JsonObject> call = getRetrofitInterface()
                .getProductData(
                        postId, userAuth.getUserId(), userAuth.getLoginKey());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(responseBody);
                } else {
                    listener.onFail(bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(t.getMessage());
            }
        });

        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall getUserDetails(String userId, UserAuth userAuth,
            final UserDetailsListener listener) {

        Call<JsonObject> call;
        call = getRetrofitInterface().getUserDetails(
                userId, userAuth.getUserId(), userAuth.getLoginKey());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(responseBody);
                } else {
                    listener.onFail(bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall getProductComments(String postId, UserAuth userAuth,
            final ProductCommentsListener listener) {

        Call<JsonObject> call;
        call = getRetrofitInterface().getProductComments(
                postId, userAuth.getUserId(), userAuth.getLoginKey());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(responseBody);
                } else {
                    listener.onFail(bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall toggleUserFollow(final boolean isToBeFollowed, String userId, UserAuth userAuth,
            final ToggleUserFollowListener listener) {

        Call<JsonObject> call;
        if (isToBeFollowed) {
            call = getRetrofitInterface().followUser(
                    userId, userAuth.getUserId(), userAuth.getLoginKey());
        } else {
            call = getRetrofitInterface().unfollowUser(
                    userId, userAuth.getUserId(), userAuth.getLoginKey());
        }
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(isToBeFollowed, responseBody);
                } else {
                    listener.onFail(isToBeFollowed, bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(isToBeFollowed, t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall toggleProductLike(final boolean isToBeLiked, String postId, UserAuth userAuth,
            final ToggleProductLikeListener listener) {

        Call<JsonObject> call;
        if (isToBeLiked) {
            call = getRetrofitInterface().likeProduct(
                    postId, userAuth.getUserId(), userAuth.getLoginKey());
        } else {
            call = getRetrofitInterface().unlikeProduct(
                    postId, userAuth.getUserId(), userAuth.getLoginKey());
        }
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(isToBeLiked, responseBody);
                } else {
                    listener.onFail(isToBeLiked, bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(isToBeLiked, t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall reportProduct(String postId, UserAuth userAuth,
            final ReportProductListener listener) {

        Call<JsonObject> call;
        call = getRetrofitInterface().reportProduct(
                postId, userAuth.getUserId(), userAuth.getLoginKey());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(responseBody);
                } else {
                    listener.onFail(bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall addProductToCart(String postId, UserAuth userAuth,
            final AddProductToCartListener listener) {

        Call<JsonObject> call;
        call = getRetrofitInterface()
                .addToCart(postId, userAuth.getUserId(), userAuth.getLoginKey());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(responseBody);
                } else {
                    listener.onFail(bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }

    @Override
    public ApiCall addOrEditPost(final boolean isBeingAdded, String postId, String title,
            String description, String price, String purchasePrice, String brandId,
            String brandName, String colorId, String sizeId, String newWithTags, String categoryId,
            String addressId, UserAuth userAuth, final AddOrEditProductListener listener) {

        Call<JsonObject> call;
        if (isBeingAdded) {
            call = getRetrofitInterface().addProduct(
                    postId, title, description, price, purchasePrice, brandId, brandName, colorId,
                    sizeId, newWithTags, categoryId, addressId, userAuth.getUserId(),
                    userAuth.getLoginKey());
        } else {
            call = getRetrofitInterface().editProduct(
                    postId, title, description, price, purchasePrice, brandId, brandName, colorId,
                    sizeId, newWithTags, categoryId, addressId, userAuth.getUserId(),
                    userAuth.getLoginKey());
        }
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(retrofit.Response<JsonObject> response,
                    retrofit.Retrofit retrofit) {

                JsonObject responseBody = response.body();
                String bodyString = responseBody.toString();
                Log.d(TAG, "body : ".concat(bodyString));

                if (isStatusSuccess(responseBody)) {
                    listener.onSuccess(isBeingAdded, responseBody);
                } else {
                    listener.onFail(isBeingAdded, bodyString);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFail(isBeingAdded, t.getMessage());
            }
        });
        return new RetrofitApiCall(call);
    }
}