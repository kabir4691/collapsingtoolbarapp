package com.kabir.example.utils.api;

import com.android.volley.toolbox.StringRequest;
import com.kabir.example.utils.api.ApiCall;

/**
 * Created by Kabir on 27-11-2015.
 */
public class VolleyApiCall implements ApiCall {

    StringRequest request;

    public VolleyApiCall(StringRequest request) {
        this.request = request;
    }

    @Override
    public void cancel() {
        request.cancel();
    }
}
