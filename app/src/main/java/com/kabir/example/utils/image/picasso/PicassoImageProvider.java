package com.kabir.example.utils.image.picasso;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.kabir.example.R;
import com.kabir.example.utils.image.ImageProvider;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Created by Kabir on 25-11-2015.
 */
public class PicassoImageProvider implements ImageProvider {

    Picasso picasso;
    private int placeHolderResourceId = R.drawable.image_placeholder_square;
    private int errorResourceId = R.drawable.image_placeholder_square;

    public PicassoImageProvider(Activity activity) {
        picasso = Picasso.with(activity);
    }

    public PicassoImageProvider(Fragment fragment) {
        picasso = Picasso.with(fragment.getContext());
    }

    @Override
    public void displayImage(String url, ImageView imageView) {

        RequestCreator requestCreator = picasso.load(url);
        requestCreator = loadSettings(requestCreator);
        requestCreator.into(imageView);
    }

    @Override
    public void displayImage(String url, int width, int height, ImageView imageView) {

        RequestCreator requestCreator = picasso.load(url);
        requestCreator = loadSettings(requestCreator);
        requestCreator.resize(width, height);
        requestCreator.into(imageView);
    }

    private RequestCreator loadSettings(RequestCreator creator) {

        if (placeHolderResourceId != -1) {
            creator.placeholder(placeHolderResourceId);
        }
        if (errorResourceId != -1) {
            creator.error(errorResourceId);
        }
        return creator;
    }

    @Override
    public void setPlaceHolderResourceId(int placeHolderResourceId) {
        this.placeHolderResourceId = placeHolderResourceId;
    }

    @Override
    public void setErrorResourceId(int errorResourceId) {
        this.errorResourceId = errorResourceId;
    }

    @Override
    public void clearMemory() {
    }
}
