package com.kabir.example.utils;

import android.support.annotation.NonNull;

public class IntegerUtils {

    /**
     * Calculates the discount from the selling price and purchase price.
     *
     * @param sellingPrice  The selling price.
     * @param purchasePrice The purchase price or MRP.
     * @return The calculated discount.
     */
    public static int calculateDiscount(@NonNull int sellingPrice, @NonNull int purchasePrice) {

        if (sellingPrice == 0) {
            return 100;
        } else if (purchasePrice == 0) {
            return 0;
        } else {
            return (int) ((1 - ((float) sellingPrice / (float) purchasePrice)) * 100);
        }
    }
}
