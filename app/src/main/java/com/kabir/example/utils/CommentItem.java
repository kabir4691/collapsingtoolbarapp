package com.kabir.example.utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.kabir.example.utils.api.ApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class CommentItem implements Parcelable {

    private String commentId;
    private String imageUrl;
    private String userId;
    private String userName;
    private String comment;
    private String timestamp;
    private String duration;
    private Boolean isOwnComment;

    public CommentItem() {
    }

    public static CommentItem parseJSON(JSONObject jsonObject) throws JSONException {

        CommentItem item = new CommentItem();

        item.commentId = jsonObject.getString(ApiResponse.KEY_ID);
        item.imageUrl = jsonObject.getString(ApiResponse.KEY_PICTURE);
        item.userId = jsonObject.getString(ApiResponse.KEY_USER_ID);
        item.userName = Html.fromHtml(jsonObject.getString(ApiResponse.KEY_USERNAME)).toString();
        item.comment = Html.fromHtml(jsonObject.getString(ApiResponse.KEY_TEXT)).toString();
        item.timestamp = jsonObject.getString(ApiResponse.KEY_TIMESTAMP);

        String durationString = StringUtils.calculateDurationFromTimestamp(item.timestamp);
        if (durationString.equals("0s")) {
            item.duration = "Just now";
        } else {
            item.duration = durationString.concat(" ago");
        }

        item.isOwnComment = false;
        return item;
    }

    public CommentItem(Parcel parcel) {

        commentId = parcel.readString();
        imageUrl = parcel.readString();
        userId = parcel.readString();
        userName = parcel.readString();
        comment = parcel.readString();
        timestamp = parcel.readString();
        duration = parcel.readString();
        isOwnComment = (Boolean) parcel.readValue(null);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(commentId == null ? "" : commentId);
        dest.writeString(imageUrl == null ? "" : imageUrl);
        dest.writeString(userId == null ? "" : userId);
        dest.writeString(userName == null ? "" : userName);
        dest.writeString(comment == null ? "" : comment);
        dest.writeString(timestamp == null ? "" : timestamp);
        dest.writeString(duration == null ? "" : duration);
        dest.writeValue(isOwnComment == null ? false : isOwnComment);
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    public static final Parcelable.Creator<CommentItem> CREATOR = new Parcelable
            .Creator<CommentItem>() {

        public CommentItem createFromParcel(Parcel in) {
            return new CommentItem(in);
        }

        public CommentItem[] newArray(int size) {
            return new CommentItem[size];
        }
    };

    public String getCommentId() {
        return commentId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getComment() {
        return comment;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getDuration() {
        return duration;
    }

    public boolean isOwnComment() {
        return isOwnComment;
    }
}
