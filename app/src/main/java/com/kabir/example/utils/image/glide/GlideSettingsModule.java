package com.kabir.example.utils.image.glide;

import android.content.Context;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.GlideModule;

/**
 * Created by Kabir on 27-11-2015.
 */
public class GlideSettingsModule implements GlideModule {

    private final String TAG = "GlideSettingsModule";

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {

        Log.d(TAG, "applyOptions");

        GlideCalculator calculator = new GlideCalculator(
                context, DecodeFormat.PREFER_RGB_565);

        builder.setDecodeFormat(DecodeFormat.PREFER_RGB_565);
        builder.setDiskCache(new InternalCacheDiskCacheFactory(
                context, "elanic", 1024 * 1024 * 200));
        builder.setMemoryCache(new LruResourceCache(calculator.getMemoryCacheSize()));
        builder.setBitmapPool(new LruBitmapPool(calculator.getBitmapPoolSize()));
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }
}

