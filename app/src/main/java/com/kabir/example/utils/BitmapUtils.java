package com.kabir.example.utils;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import rx.Observable;
import rx.Subscriber;

public class BitmapUtils {

    static String TAG = "BitmapUtils";

    /**
     * Returns the bitmap from the given file. If either the width or height (in pixels) of the
     * resulting bitmap exceeds {@linkplain AppConfig#SIZE_MAXIMUM_IMAGE_BITMAP}, then the bitmap is
     * subsampled till both its width and height are lesser than
     * {@linkplain AppConfig#SIZE_MAXIMUM_IMAGE_BITMAP}. The subsampling ratio will be a power of 2.
     *
     * @param file The File containing the image whose bitmap is to be returned.
     * @return The Bitmap obtained from the file.
     */
    public static Bitmap getBitmapFromFile(File file) throws IOException {

        BitmapFactory.Options options1 = new BitmapFactory.Options();
        options1.inJustDecodeBounds = true;

        FileInputStream fileInputStream = new FileInputStream(file);
        BitmapFactory.decodeStream(fileInputStream, null, options1);
        fileInputStream.close();

        int ratio = 1;
        if (options1.outHeight > AppConfig.SIZE_MAXIMUM_IMAGE_BITMAP || options1.outWidth >
                AppConfig
                        .SIZE_MAXIMUM_IMAGE_BITMAP) {
            ratio = (int) Math.pow(2, (int) Math.ceil(Math.log(
                    AppConfig.SIZE_MAXIMUM_IMAGE_BITMAP / (double) Math.max(
                            options1.outHeight, options1.outWidth)) / Math.log(0.5)));
        }

        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = ratio;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options2);

//        Log.d(TAG, "old width and height : " + options1.outWidth + " " + options1.outHeight);
//        Log.d(TAG, "new width and height : " + options2.outWidth + " " + options2.outHeight);
//        Log.d(TAG, "ratio : " + ratio);

        return bitmap;
    }

    /**
     * Returns the bitmap from the given file. If either the width or height (in pixels) of the
     * resulting
     * bitmap exceeds {@linkplain AppConfig#SIZE_MAXIMUM_IMAGE_BITMAP}, then the bitmap is
     * subsampled till
     * both its width and height are lesser than {@linkplain AppConfig#SIZE_MAXIMUM_IMAGE_BITMAP}
     * . The
     * subsampling ratio will be a power of 2.
     *
     * @param uri     The Uri of the image whose bitmap is to be returned.
     * @param context The context to obtain the ContentResolver instance from.
     * @return The Bitmap obtained from the Uri.
     */
    public static Bitmap getBitmapFromUri(Uri uri, Context context) throws Exception {

        BitmapFactory.Options options1 = new BitmapFactory.Options();
        options1.inJustDecodeBounds = true;
        BitmapFactory
                .decodeStream(context.getContentResolver().openInputStream(uri), null, options1);

        int ratio = 1;
        if (options1.outHeight > AppConfig.SIZE_MAXIMUM_IMAGE_BITMAP || options1.outWidth >
                AppConfig
                        .SIZE_MAXIMUM_IMAGE_BITMAP) {
            ratio = (int) Math.pow(2, (int) Math.ceil(Math.log(
                    AppConfig.SIZE_MAXIMUM_IMAGE_BITMAP / (double) Math.max(
                            options1.outHeight, options1.outWidth)) / Math.log(0.5)));
        }

        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = ratio;
        Bitmap bitmap = BitmapFactory.decodeStream(
                context.getContentResolver().openInputStream(uri), null, options2);

//        Log.d(TAG, "old width and height : " + options1.outWidth + " " + options1.outHeight);
//        Log.d(TAG, "new width and height : " + options2.outWidth + " " + options2.outHeight);
//        Log.d(TAG, "ratio : " + ratio);

        return bitmap;
    }

    /**
     * Encodes the given Bitmap to its Base-64 representation. If the size (in
     * bytes) of the bitmap exceeds {@linkplain AppConfig#SIZE_MAXIMUM_IMAGE_FILE}, then the bitmap
     * is first compressed to the size of {@linkplain AppConfig#SIZE_MAXIMUM_IMAGE_FILE} and then
     * encoded.
     *
     * @param bitmap The Bitmap to encode.
     * @return String containing the Base-64 representation of the Bitmap.
     */
    public static String encodeToBase64(Bitmap bitmap) {

        ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
//        Log.d(TAG, "bitmap bytecount : " + String.valueOf(bitmap.getByteCount()));

        // No More Resize
        int mCompressPercent = 100;
//        int mCompressPercent = bitmap.getByteCount() > AppConfig.SIZE_MAXIMUM_IMAGE_FILE ? (int)
//                ((AppConfig.SIZE_MAXIMUM_IMAGE_FILE / bitmap.getByteCount()) * 100) : 100;
//        Log.d(TAG, "compress percent : " + mCompressPercent);
        bitmap.compress(Bitmap.CompressFormat.JPEG, mCompressPercent, mByteArrayOutputStream);
        byte[] mByteArray = mByteArrayOutputStream.toByteArray();
        String base64 = AppConfig.HEADER_BASE64 + Base64.encodeToString(mByteArray, Base64.DEFAULT);
//        Log.d(TAG, "base64 length : " + base64.length());

        return base64;
    }

    /**
     * Write the given bitmap into the given file. JPEG is used as the compression format with
     * quality set
     * to 100.
     *
     * @param bm   The bitmap.
     * @param file The file to write the bitmap into.
     */
    public static void writeBitmapToFile(Bitmap bm, File file) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        fos.flush();
        fos.close();
    }

    /**
     * Rotates a bitmap (whose image's orientation is specified in its EXIF data) appropriately.
     *
     * @param bitmap      The bitmap of the image.
     * @param orientation The orientation specified in EXIF.
     * @return Bitmap with correct orientation.
     */
    public static Bitmap rotateBitmapForExif(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {

            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:

                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:

                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:

                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        Bitmap bmRotated = Bitmap.createBitmap(
                bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return bmRotated;
    }

    /**
     * Get the file path from the given Uri.
     *
     * @param context The context of the calling activity.
     * @param uri     The Uri whose file path is returned.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getFilePathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
            String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {

            cursor = context.getContentResolver()
                            .query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {

                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String encodeToBase64(String fileName) throws IOException {

        InputStream inputStream = new FileInputStream(fileName);
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        byte[] bytes = output.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    /**
     * Convert the given View to its BitmapDrawable representation.
     *
     * @param view The View to convert to a BitmapDrawable.
     * @return The BitmapDrawable representation of the view.
     */
    public static BitmapDrawable convertViewToBitmapDrawable(View view) {

        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap b1 = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                                       Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b1);
        c.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(c);
        view.setDrawingCacheEnabled(true);
        Bitmap b2 = view.getDrawingCache().copy(Bitmap.Config.ARGB_8888, true);
        BitmapDrawable bd = new BitmapDrawable(view.getContext().getResources(), b2);
        view.destroyDrawingCache();
        return bd;
    }

    public static Bitmap addPadding(@NonNull Bitmap bmp, int color) {

        int biggerParam = Math.max(bmp.getWidth(), bmp.getHeight());
        Bitmap bitmap = Bitmap.createBitmap(biggerParam, biggerParam, bmp.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);

        int top = bmp.getHeight() > bmp.getWidth() ? 0 : (bmp.getWidth() - bmp.getHeight())/2;
        int left = bmp.getWidth() > bmp.getHeight() ? 0 : (bmp.getHeight() - bmp.getWidth())/2;

        canvas.drawBitmap(bmp, left, top, null);
        return bitmap;
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Observable<Bitmap> rotateBitmapRx(final Bitmap source, final float angle) {
        return Observable.create(new Observable.OnSubscribe<Bitmap>() {
            @Override
            public void call(Subscriber<? super Bitmap> subscriber) {
                subscriber.onNext(rotateBitmap(source, angle));
            }
        });
    }

    /**
     * Write the given bitmap into the given file. JPEG is used as the compression format
     *
     * @param bm   The bitmap.
     * @param file The file to write the bitmap into.
     * @param quality Quality of the image
     */
    public static boolean saveBitmapToFile(Bitmap bm, File file, int quality) throws IOException {

        FileOutputStream fos = new FileOutputStream(file);
        bm.compress(Bitmap.CompressFormat.JPEG, quality, fos);
        fos.flush();
        fos.close();

        return true;
    }

    public static Observable<Boolean> writeBitmapToFile(final Bitmap bitmap, final File file, final int quality) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean success = saveBitmapToFile(bitmap, file, quality);
                    subscriber.onNext(success);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static File getNewTimestampFile(String suffix) throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());

        String path = Environment.getExternalStorageDirectory()
                + "/" + AppConfig.NAME_FOLDER_IMAGES + "/"
                + AppConfig.NAME_FILE_IMAGES + timestamp + suffix;
        File file = new File(path);
        file.createNewFile();
        return file;
    }

    public static Observable<Bitmap> readBitmapFromFileRx(final String path,
                                                          final boolean downSample,
                                                          final int MAX_DIMEN) {
        return Observable.create(new Observable.OnSubscribe<Bitmap>() {
            @Override
            public void call(Subscriber<? super Bitmap> subscriber) {
                subscriber.onNext(readBitmapFromFile(path, downSample, MAX_DIMEN));
            }
        });
    }

    public static Bitmap autoExif(@NonNull Bitmap bitmap, @NonNull String path) {
        ExifInterface exif;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
            return bitmap;
        }

        return rotateBitmapForExif(bitmap,
                exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL));
    }

    public static Bitmap readBitmapFromFile(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        if (bitmap != null) {
            return autoExif(bitmap, path);
        } else {
            return null;
        }
    }

    public static Bitmap readBitmapFromFile(String path, boolean downSample, int MAX_DIMEN) {
        if (!downSample) {
            return readBitmapFromFile(path);
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        int height = options.outHeight;
        int width = options.outWidth;

        if (Math.max(height, width) <= MAX_DIMEN) {
            // No down-sampling required
            return readBitmapFromFile(path);
        }

        BitmapFactory.Options loadingOptions = getLoadingOptions(options, MAX_DIMEN);

        Bitmap bitmap = BitmapFactory.decodeFile(path, loadingOptions);
        if (bitmap == null) {
            return null;
        }

        bitmap = autoExif(bitmap, path);

        // check and resize to fit exactly
        int maxSize = Math.max(bitmap.getWidth(), bitmap.getHeight());
        if (maxSize <= MAX_DIMEN) {
            // No need to resize
            return bitmap;
        }

        float samplingFactor = (float)MAX_DIMEN/maxSize;

        bitmap = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth() * samplingFactor),
                (int)(bitmap.getHeight() * samplingFactor), true);

        return bitmap;
    }

    public static BitmapFactory.Options getLoadingOptions(BitmapFactory.Options options, int MAX_DIMENS) {
        return getLoadingOptions(options, MAX_DIMENS, false);
    }

    public static BitmapFactory.Options getLoadingOptions(BitmapFactory.Options options,
                                                          int MAX_DIMENS, boolean forecSampling) {
        int width = options.outWidth;
        int height = options.outHeight;

        float insampleSize = 1.0f;
        if (width > height) {
            insampleSize = (float)width/(float)MAX_DIMENS;
        } else {
            insampleSize = (float)height/(float)MAX_DIMENS;
        }

        if (insampleSize < 1.0) {
            insampleSize = 1.0f;
        }

        BitmapFactory.Options loadOptions = new BitmapFactory.Options();
        if (insampleSize > 2 && forecSampling) {
            int power = (int)Math.ceil(insampleSize);
            power--;
            power |= power >> 1;
            power |= power >> 2;
            power |= power >> 4;
            power |= power >> 8;
            power |= power >> 16;
            power++;

            loadOptions.inSampleSize = power;
        } else {
            loadOptions.inSampleSize = (int)Math.ceil(insampleSize);
        }

        return loadOptions;
    }
}
