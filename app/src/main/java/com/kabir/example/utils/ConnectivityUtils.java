package com.kabir.example.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityUtils {

    /**
     * Check if the device is connected to the Internet through any means.
     *
     * @return true if the device has Internet connectivity, false otherwise.
     */
    public static boolean isConnectedToInternet() {

        boolean isConnected = false;

        NetworkInfo[] networkInfo = ((ConnectivityManager) AppContext.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo();
        for (NetworkInfo networkInfoItem : networkInfo) {
            isConnected = (isConnected || networkInfoItem.isConnected());
        }
        return isConnected;
    }
}
