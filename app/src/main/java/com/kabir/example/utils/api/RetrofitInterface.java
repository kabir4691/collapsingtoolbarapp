package com.kabir.example.utils.api;

import com.google.gson.JsonObject;
import com.kabir.example.utils.api.ApiParameter;
import com.kabir.example.utils.api.ApiUrl;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface RetrofitInterface {

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PRODUCT_DETAILS)
    Call<JsonObject> getProductData(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PRODUCT_LIKE)
    Call<JsonObject> likeProduct(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PRODUCT_UNLIKE)
    Call<JsonObject> unlikeProduct(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PROFILE_FOLLOW)
    Call<JsonObject> followUser(
            @Field(ApiParameter.KEY_USER_ID2) String userId2,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PROFILE_UNFOLLOW)
    Call<JsonObject> unfollowUser(
            @Field(ApiParameter.KEY_USER_ID2) String userId2,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PROFILE_DETAILS)
    Call<JsonObject> getUserDetails(
            @Field(ApiParameter.KEY_USER_ID2) String userId2,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PRODUCT_COMMENTS)
    Call<JsonObject> getProductComments(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_CART_ADD)
    Call<JsonObject> addToCart(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_PRODUCT_REPORT)
    Call<JsonObject> reportProduct(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_SELL_POST_ADD)
    Call<JsonObject> addProduct(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_TITLE) String title,
            @Field(ApiParameter.KEY_DESCRIPTION) String description,
            @Field(ApiParameter.KEY_PRICE) String price,
            @Field(ApiParameter.KEY_PURCHASE_PRICE) String purchasePrice,
            @Field(ApiParameter.KEY_BRAND_ID) String brandId,
            @Field(ApiParameter.KEY_BRAND_NAME) String brandName,
            @Field(ApiParameter.KEY_COLOR_ID) String colorId,
            @Field(ApiParameter.KEY_SIZE_ID) String sizeId,
            @Field(ApiParameter.KEY_NWT) String nwt,
            @Field(ApiParameter.KEY_CATEGORY_ID) String categoryId,
            @Field(ApiParameter.KEY_ADDRESS_ID) String addressId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);

    @FormUrlEncoded
    @POST(ApiUrl.ENDPOINT_SELL_POST_EDIT)
    Call<JsonObject> editProduct(
            @Field(ApiParameter.KEY_POST_ID) String postId,
            @Field(ApiParameter.KEY_TITLE) String title,
            @Field(ApiParameter.KEY_DESCRIPTION) String description,
            @Field(ApiParameter.KEY_PRICE) String price,
            @Field(ApiParameter.KEY_PURCHASE_PRICE) String purchasePrice,
            @Field(ApiParameter.KEY_BRAND_ID) String brandId,
            @Field(ApiParameter.KEY_BRAND_NAME) String brandName,
            @Field(ApiParameter.KEY_COLOR_ID) String colorId,
            @Field(ApiParameter.KEY_SIZE_ID) String sizeId,
            @Field(ApiParameter.KEY_NWT) String nwt,
            @Field(ApiParameter.KEY_CATEGORY_ID) String categoryId,
            @Field(ApiParameter.KEY_ADDRESS_ID) String addressId,
            @Field(ApiParameter.KEY_USER_ID) String userId,
            @Field(ApiParameter.KEY_LOGIN_KEY) String loginKey);
}
