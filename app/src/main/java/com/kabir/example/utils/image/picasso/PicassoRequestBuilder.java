package com.kabir.example.utils.image.picasso;

import com.kabir.example.utils.image.RequestBuilder;
import com.squareup.picasso.RequestCreator;

/**
 * Created by Kabir on 27-11-2015.
 */
public class PicassoRequestBuilder implements RequestBuilder {

    RequestCreator requestCreator;

    public PicassoRequestBuilder(RequestCreator requestCreator) {
        this.requestCreator = requestCreator;
    }

    @Override
    public void setPlaceHolder(int resourceId) {
        requestCreator.placeholder(resourceId);
    }

    @Override
    public void setError(int resourceId) {
        requestCreator.error(resourceId);
    }
}
