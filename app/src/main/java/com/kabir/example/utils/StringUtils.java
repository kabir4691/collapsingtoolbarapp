package com.kabir.example.utils;

import android.support.annotation.NonNull;
import android.text.Html;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class StringUtils {

    public static final String RUPEE_SYMBOL = "₹";

    /**
     * @param str The String to check.
     * @return true if the given string is either empty or only contains space(s), false otherwise.
     */
    public static boolean isBlank(@NonNull String str) {

        if (str.isEmpty()) {
            return true;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String capWords(@NonNull String source) {

        if (source.isEmpty()) {
            return source;
        }

        StringBuilder result = new StringBuilder();
        String[] words = source.split(" ");
        for (String word : words) {

            char[] charArray = word.trim().toCharArray();
            if (charArray.length == 0) {
                continue;
            }
            charArray[0] = Character.toUpperCase(charArray[0]);
            word = new String(charArray);
            result.append(word).append(" ");
        }
        return result.toString();
    }


    /**
     * Calculate the time elapsed from the point of time specified by the timestamp and return it
     * as a pre-formatted String. Returns "0s" if the calculated duration is 0 or a negative
     * number. Returns null if any parsing error occurs.
     *
     * @return The String containing the elapsed time.
     */
    public static String calculateDurationFromTimestamp(@NonNull String timestamp) {

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            Date todayDate = new Date();
            Date postedDate = dateFormat.parse(timestamp);
            long elapsedTime = todayDate.getTime() - postedDate.getTime();

            int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(elapsedTime);

            if (seconds <= 0) {
                return "0s";
            }

            if (seconds < 60) {
                return String.valueOf(seconds) + "s";
            }

            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(elapsedTime);
            if (minutes < 60) {
                return String.valueOf(minutes) + "m";
            }

            int hours = (int) TimeUnit.MILLISECONDS.toHours(elapsedTime);
            if (hours < 24) {
                return String.valueOf(hours) + "h";
            }

            int days = (int) TimeUnit.MILLISECONDS.toDays(elapsedTime);
            if (days < 7) {
                return String.valueOf(days) + "d";
            }

            return String.valueOf(days / 7) + "w";

        } catch (ParseException e) {

            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get the text to be displayed for number of likes.
     *
     * @param count   The current likes count.
     * @param isLiked Whether the user himself has liked this product.
     */
    public static String getTextForLikesCount(@NonNull int count, @NonNull boolean isLiked) {

        switch (count) {

            case 0:
                return "Be the first to like this!";
            case 1:
                return isLiked ? "You like this" : "1 user likes this";
            case 2:
                return isLiked ? "You and 1 user like this" : count + " users like this";
            default:
                return isLiked ? "You and " + count + " users like this" :
                        count + " users like this";
        }
    }

    public static String removeSlashFromStart(@NonNull String source) {
        if (source.startsWith("/")) {
            return source.substring(1, source.length());
        }

        return source;
    }

    /**
     * @param hm The HashMap.
     * @return The key-value pairs from a HashMap in the form of a String.
     */
    public static String getKeyValuePairsFromHashMap(HashMap<String, String> hm) {

        String values = "";
        Iterator it = hm.keySet().iterator();
        while (it.hasNext()) {

            String key = (String) it.next();
            String value = hm.get(key);
            values = values.concat("Key: " + key + " Value: " + value + "\n");
        }
        return values;
    }

    public static String fromHtml(String source, String defaultVal) {
        if (source == null || source.isEmpty()) {
            return defaultVal;
        }

        return Html.fromHtml(source).toString();
    }
}
