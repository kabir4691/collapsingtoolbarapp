package com.kabir.example.utils.image;

import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by Kabir on 27-11-2015.
 */
public interface ImageProvider {

    void displayImage(String url, ImageView imageView);
    void displayImage(String url, int width, int height, ImageView imageView);

    void setPlaceHolderResourceId(int placeHolderResourceId);
    void setErrorResourceId(int errorResourceId);
//    RequestBuilder loadSettings(RequestBuilder requestBuilder);

//    void setCacheStrategy(DiskCacheStrategy cacheStrategy);
    void clearMemory();
}
