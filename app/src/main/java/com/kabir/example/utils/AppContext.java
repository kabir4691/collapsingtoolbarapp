package com.kabir.example.utils;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import com.bumptech.glide.Glide;

/**
 * A singleton instance of the application. Can be used to get the application context wherever
 * you want in the code.
 */
public class AppContext extends Application {

    private static final String TAG = "AppContext";
    static AppContext mInstance = null;

    private Handler memoryHandler;
    private static final long MEMORY_CHECK_INTERVAL = 5000; // 5sec
    private static final float LOW_MEMORY_THRESHOLD = 20f; // 20%

    public static synchronized AppContext getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        memoryHandler = new Handler();
        startMemoryAnalyzer();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mInstance = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        clearMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.get(this).trimMemory(level);
    }

    private void clearMemory() {
        Log.e(TAG, "clear memory called");
        Glide.get(this).clearMemory();
    }

    private void startMemoryAnalyzer() {
        memoryHandler.postDelayed(memoryRunnable, MEMORY_CHECK_INTERVAL);
    }

    private Runnable memoryRunnable = new Runnable() {
        @Override
        public void run() {
            float availableMemory = MemoryUtils.getPercentAvailableMemory();
            if (availableMemory <= LOW_MEMORY_THRESHOLD) {
                Log.i(TAG, "available memory: " + availableMemory);
                clearMemory();
            }

            startMemoryAnalyzer();
        }
    };
}