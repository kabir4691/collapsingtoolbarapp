package com.kabir.example.utils;

import com.afollestad.materialdialogs.MaterialDialog;

public class DialogUtils {

    /**
     * Show the MaterialDialog with the given message.
     *
     * @param dialog  The MaterialDialog.
     * @param message The message to display.
     */
    public static void showProgressDialog(MaterialDialog dialog, String message) {

        try {
            if (dialog != null) {

                dialog.setContent(message);
                dialog.show();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the MaterialDialog shown by calling {@link #showProgressDialog}.
     */
    public static void hideProgressDialog(MaterialDialog dialog) {

        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
