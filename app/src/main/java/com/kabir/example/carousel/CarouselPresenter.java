package com.kabir.example.carousel;

import android.os.Bundle;

/**
 * Created by Kabir on 25-11-2015.
 */
public interface CarouselPresenter {

    boolean attachView(Bundle extras);
    void detachView();

    void setImageUrl(String imageUrl);
    String getImageUrl();

    void setPosition(int position);
    int getPosition();
}
