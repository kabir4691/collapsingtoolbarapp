package com.kabir.example.carousel;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kabir.example.main.ProductImageItem;

import java.util.ArrayList;

public class CarouselPagerAdapter extends FragmentPagerAdapter implements CarouselPageFragment
        .Callback {

    private ArrayList<ProductImageItem> mList;
    private boolean mIsGalleryShown;
    private Callback mCallback;

    public CarouselPagerAdapter(ArrayList<ProductImageItem> list, FragmentManager fm) {
        super(fm);
        mList = list;
    }

    @Override
    public Fragment getItem(int position) {

        CarouselPageFragment fragment = CarouselPageFragment
                .newInstance(position, mList.get(position).getMediumSizeUrl());
        fragment.setCallback(this);
        return fragment;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public void onFragmentClick(int position) {

        if (isGalleryShown()) {
            if (mCallback != null) {
                mCallback.onPageClick(position);
            }
        }
    }

    /**
     * Set whether the gallery displaying full screen images should be shown on clicking an item.
     *
     * @param isGalleryShown True if the gallery should be shown, false otherwise.
     */
    public void setGalleryShown(boolean isGalleryShown) {
        mIsGalleryShown = isGalleryShown;
    }

    public boolean isGalleryShown() {
        return mIsGalleryShown;
    }

    public void setCallback(final Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        void onPageClick(int position);
    }
}
