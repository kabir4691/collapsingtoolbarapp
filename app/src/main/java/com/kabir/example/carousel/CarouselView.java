package com.kabir.example.carousel;

/**
 * Created by Kabir on 25-11-2015.
 */
public interface CarouselView {

    String KEY_POSITION = "position";
    String KEY_IMAGE_URL = "imageUrl";

    void onFatalError();
}
