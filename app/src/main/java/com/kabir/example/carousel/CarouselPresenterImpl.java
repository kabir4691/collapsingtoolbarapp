package com.kabir.example.carousel;

import android.os.Bundle;
import android.util.Log;

/**
 * Created by Kabir on 25-11-2015.
 */
public class CarouselPresenterImpl implements CarouselPresenter {

    private final String TAG = "CarouselPresenterImpl";

    private CarouselView mView;

    private String imageUrl;
    private int position;

    CarouselPresenterImpl(CarouselView view) {
        mView = view;
    }

    @Override
    public boolean attachView(Bundle extras) {

        if (extras == null || extras.isEmpty()) {
            Log.e(TAG, "extras is null or empty");
            return false;
        }

        String url = extras.getString(CarouselView.KEY_IMAGE_URL);
        if (url == null || url.isEmpty()) {
            Log.e(TAG, "image url is null or empty");
            return false;
        }
        setImageUrl(url);

        int pos = extras.getInt(CarouselView.KEY_POSITION, -1);
        if (pos == -1) {
            Log.e(TAG, "position is not specified");
            return false;
        }
        setPosition(pos);
        return true;
    }

    @Override
    public void detachView() {

    }

    @Override
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int getPosition() {
        return position;
    }
}
