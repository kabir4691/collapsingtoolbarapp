package com.kabir.example.carousel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kabir.example.R;
import com.kabir.example.utils.image.ImageProvider;
import com.kabir.example.utils.image.picasso.PicassoImageProvider;

public class CarouselPageFragment extends Fragment implements CarouselView {

    private static final String TAG = "CarouselPageFragment";

    private CarouselPresenter mPresenter;
    private Callback mCallback;
    private ImageProvider mImageProvider;

    public static CarouselPageFragment newInstance(int position, String imageUrl) {

        CarouselPageFragment fragment = new CarouselPageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_POSITION, position);
        bundle.putString(KEY_IMAGE_URL, imageUrl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onFatalError() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImageProvider = new PicassoImageProvider(this);
        mPresenter = new CarouselPresenterImpl(this);
        if (!mPresenter.attachView(getArguments())) {
            onFatalError();
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.page_product_carousel, container, false);

        ImageView iv_image = (ImageView) rootView.findViewById(R.id.iv_image);

        mImageProvider.displayImage(mPresenter.getImageUrl(), iv_image);

        rootView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mCallback != null) {
                    mCallback.onFragmentClick(mPresenter.getPosition());
                }
            }
        });

        return rootView;
    }

    public void setCallback(final Callback onClickListener) {
        mCallback = onClickListener;
    }

    public interface Callback {
        void onFragmentClick(int position);
    }
}
