package com.kabir.example.thumbnail;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ThumbnailItemDecoration extends RecyclerView.ItemDecoration {

    int mInset;

    /**
     * @param inset The inset width(in pixels) between the items.
     */
    public ThumbnailItemDecoration(int inset) {
        mInset = inset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
            RecyclerView.State state) {

        if (parent.getChildPosition(view) != 0) {
            outRect.left = mInset;
        }
    }
}