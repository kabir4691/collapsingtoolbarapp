package com.kabir.example.thumbnail;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kabir.example.R;
import com.kabir.example.main.ProductImageItem;
import com.kabir.example.utils.image.ImageProvider;
import com.kabir.example.utils.image.picasso.PicassoImageProvider;

import java.util.ArrayList;

public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.ViewHolder> {

    AppCompatActivity mActivity;
    ImageProvider mImageProvider;
    ArrayList<ProductImageItem> mList;
    LayoutInflater mInflater;
    CustomOnItemClickListener mOnItemClickListener;
    Resources mResources;

    public ThumbnailAdapter(AppCompatActivity activity, ArrayList<ProductImageItem> list) {

        mActivity = activity;
        mImageProvider = new PicassoImageProvider(activity);
        mList = list;
        mInflater = LayoutInflater.from(activity);
        mResources = activity.getResources();
    }

    @Override
    public ThumbnailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.item_product_thumbnail, null);
        return new ViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        ProductImageItem item = mList.get(position);

        int sdk = Build.VERSION.SDK_INT;
        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
            viewHolder.imageView.setBackgroundDrawable(
                    item.isSelected() ? mResources
                            .getDrawable(R.drawable.background_product_rv_thumbnail_item) : null);
        } else {
            viewHolder.imageView.setBackground(
                    item.isSelected() ? mResources.getDrawable(
                            R.drawable.background_product_rv_thumbnail_item) : null);
        }

        mImageProvider.displayImage(item.getThumbnailUrl(), viewHolder.imageView);

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.iv_image);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface CustomOnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final CustomOnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public ArrayList<ProductImageItem> getImagesList() {
        return mList;
    }
}
